# Params

Baud rate = 10400
Bit per frame = 8
Stop Bits = 1
Parity = none

I am using Viaken VAG KKL Cable. This will work ONLY with FT231 cables! CH340 will not work because it use raw FT231 library. It is non standard use of serial protocol and special cable is required!!

# Initializing

1. Init KLine & LLine with HIGH level for at least 1 sec.
2. Pull low KLine & LLine for 200ms (1 pulse).
3. Send address in 200ms pulses LSB first!
4. Pull KLine & LLine high. Now LLine is use less.
5. Wait ~500-1000ms for data.
6. Read data. You must find 0x55 in data.

# Frame

* First byte is always length!
* There is about 500ms delay between request and response.
* In those 500ms you must reconfigure hardware port is you need in case of UART you only need to flush input FIFO after small delay. 
* BUS is multimaster. Without collision detection.
* On the bus only Clip/XR25/NXR and device waked up using LLine/KLine talks with each other.


# Example comunication after waking up

Talking with 2.SYP. Safrane Ph2 voice synthesizer. It is status check. XR25 or other tools do such request often. Every ~1sec.

```
> {0x03, 0x31, 0x50, 0x84}
= delay 60ms (!)
< {0x09, 0x31, 0x50, 0x53, 0x00, 0x00, 0x00, 0x10, 0x1E, 0x0B}
```

LLine at this point is ignored. It can be used but it do not do anything. LLine is used for waking up.

There is no rule in frames.. Only first byte in length. Rest is needed to reverse engineered..


Language change french (#1)

```
> {0x04, 0x40, 0x01, 0x00, 0x45}
= delay 60ms
< {0x04, 0x40, 0x01, 0x00, 0x45}
```

Language change english (#2)

```
> {0x04, 0x40, 0x01, 0x01, 0x46}
= delay 60ms
< {0x04, 0x40, 0x01, 0x01, 0x46}
```

Language change italian (#4)

```
> {0x04, 0x40, 0x01, 0x03, 0x48}
= delay 60ms
< {0x04, 0x40, 0x01, 0x03, 0x48}
```

# New notes!!

Example status in 2.SYP

```
> {0x03, 0x31, 0x50, 0x84}
= delay 60ms (!)
< {0x09, 0x31, 0x50, 0x53, 0x00, 0x00, 0x00, 0x10, 0x1E, 0x0B}
```

## String from xr25pc
> 03 31 50 84 20 09 31 50 53 00 00 00 10 1E 0B
  00 01 02 03 04 05 06 07 08 09 10 11 12 13 14

[14] -> Language & version

Response:

[00] = 0x09 -> Length without this byte!
[01] = 0x31 -> ?? (Response type?)
[02] = 0x50 -> ?? (Response type?)
[03] = 0x53 -> Higher 4 bits version?, lower 4 bits language but in reality 3 bit because lowest is always 1
[04] = 0x00 -> ??
[05] = 0x00 -> LED BAR
[06] = 0x00 -> LED BAR
[08] = 0x10 -> LED BAR
[09] = 0x1E -> LED BAR
[10] = 0x0B -> LED BAR

Data:
language    = buffer[3] & 0x0F
key_rep     = (buffer[5] & 0x02) >> 1


# Speed control,

> 03 31 50 84
< 0E 31 50 77 00 84 15 59 12 00 00 FF FF FF 07
  00 01 02 03 04 05 06 07 08 09 10 11 12 13 14

77 00 84 15 59  part id
12 verion
etat ? 00
day
month
year
eeprom

16960905160
1696083344