#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <ftdi.h>

#include "xr25.h"

int xr25Init(xr25Context *ctx, int baudrate)
{
    // variables
    int ret;
    struct ftdi_version_info version;

    // assign
    ctx->usbInitialized = false;
    ctx->ftdi = NULL;
    ctx->buf = NULL;

    // io
    fprintf(stderr, "XR25: Initialization..\n");

    // ftdi init
    if ((ctx->ftdi = ftdi_new()) == 0)
    {
        fprintf(stderr, "FTDI: ftdi_new fail.\n");
        return -1; // ftdi_new fail
    }

    // ftdi version check
    version = ftdi_get_library_version();
    fprintf(stderr, "FTDI: Initialized libftdi (%s).\n", version.version_str);

    // open ftdi
    // hardcode to FTDI FT231X (ex. Viaken VSCAN Vag KKL Cable)
    // fuck the CH340 gang
    // connect only ONE device with FTDI it will select it randomly!
    // TODO: Better cable selection
    ret = ftdi_usb_open(ctx->ftdi, 0x0403, 0x6015);
    if (ret < 0)
    {
        fprintf(stderr, "FTDI: ftdi_usb_open fail.\n");
        fprintf(stderr, "FTDI: ret=%d (%s)\n", ret, ftdi_get_error_string(ctx->ftdi));
        return -2; // ftdi_usb_open fail
    }
    ctx->usbInitialized = true;
    fprintf(stderr, "FTDI: USB device connected.\n");

    // set baud rate
    ret = ftdi_set_baudrate(ctx->ftdi, baudrate); // ISO9141 Baud rate
    if (ret < 0)
    {
        fprintf(stderr, "FTDI: ftdi_set_baudrate - fail.\n");
        return -3; // ftdi_set_baudrate fail
    }

    // alloc buffer
    ctx->buf = malloc(ISO_BUFSIZE);
    if (ctx->buf == NULL)
    {
        fprintf(stderr, "LIBC: malloc fail.\n");
        return -4; // malloc fail
    }

    return 0; // success
}

int xr25SetISOState(xr25Context *ctx, bool state)
{
    // variables
    int ret;

    // set ftdi
    ret = ftdi_set_line_property2(ctx->ftdi, BITS_8, STOP_BIT_1, NONE, state ? BREAK_OFF : BREAK_ON); // OFF = High
    if (ret < 0)
    {
        fprintf(stderr, "FTDI: ftdi_set_line_property2 - fail.\n");
        return -1; // ftdi_set_line_property2 fail
    }

    return 0; // success
}

int xr25ConnectToAddress(xr25Context *ctx, int address, int *calculatorId)
{
    // variables
    int i, byte, ret, safetyCounter, orgAddr;
    unsigned char temp;
    bool connected;

    // assign
    connected = false;
    orgAddr = address;
    (*calculatorId) = 0;

    // io
    fprintf(stderr, "XR25: Connecting to 0x%02x.\n", (unsigned char)address);

    // start communication
    if (xr25SetISOState(ctx, ISO_HIGH) != EXIT_SUCCESS)
    {
        return -1; // starting fail - stage 1
    }

    usleep(1000 * 1000); // 1000ms - bus idle

    // pull low for 200ms
    if (xr25SetISOState(ctx, ISO_LOW) != EXIT_SUCCESS)
    {
        return -2; // starting fail - stage 2
    }
    usleep(200 * 1000); // 200ms - bus idle

    // send device address
    // 8 bit long
    for (i = 0; i < 8; i++)
    {
        byte = address & 0x1;                                                   // get last bit
        address = address >> 1;                                                 // move addr to right
        if (xr25SetISOState(ctx, (!byte) ? ISO_LOW : ISO_HIGH) != EXIT_SUCCESS) // sending
        {
            return -3; // starting fail - stage 3
        }
        usleep(200 * 1000); // 200ms - bus idle
    }
    // pull high
    if (xr25SetISOState(ctx, ISO_HIGH) != EXIT_SUCCESS)
    {
        return -4; // starting fail - stage 4
    }

    // io
    fprintf(stderr, "XR25: Request sent waiting for response.\n");
    usleep(750 * 1000); // 750ms - wait

    // read data one byte at once
    for (safetyCounter = 0; safetyCounter < SAFETY_COUNTER_LOW; safetyCounter++)
    {
        ret = ftdi_read_data(ctx->ftdi, ctx->buf, 1); // read one char!
        if (ret < 0)                                  // ftdi/usb error
        {
            fprintf(stderr, "XR25: Couldn't connect. Reason: FTDI error. Exiting.\n");
            return -5; // data error - ftdi
        }
        else if (ret == 0 && safetyCounter == 0) // if first read fails there is kline error
        {
            fprintf(stderr, "XR25: Couldn't connect. Reason: KLine error (No data). Exiting.\n");
            return -6; // data error - kline
        }
        else if (ret == 0 && safetyCounter != 0) // no more data!
        {
            break;
        }
        else // we read some data
        {
            if (connected) // we are connected and we searching for computer id
            {
                (*calculatorId) = ((*calculatorId) << 8) | ctx->buf[0];
            }
            if (ctx->buf[0] == 0x55 && !connected) // we are not connected and we found ack
            {
                connected = true;
            }
        }
    }

    // check status
    if (!connected)
    {
        fprintf(stderr, "XR25: Couldn't connect. Reason: KLine error (Invalid response). Exiting.\n");
        return -7; // data error - KLine #2
    }

    // get calculator ID

    // io
    fprintf(stderr, "XR25: Connected to 0x%02x.\n", (unsigned char)orgAddr);
    fprintf(stderr, "XR25: Calculator ID 0x%04x.\n", (unsigned short)(*calculatorId));
    fprintf(stderr, "XR25: Calculator name: %s\n", xr25ComputerIDToStr((*calculatorId)));

    return 0; // success
}

const char *xr25ComputerIDToStr(int calculatorId)
{
    switch (calculatorId)
    {
    case 0x7F80:
        return "[ .SYP] Voice Synthesizer for Renault Safrane Ph1";
    case 0x7086:
        return "[2.SYP] Voice Synthesizer for Renault Safrane Ph2";
    default:
        return "NOT FOUND";
    }
}

void xr25DeInit(xr25Context *ctx)
{
    // variables
    int ret;

    // io
    fprintf(stderr, "XR25: Deinitialization\n");

    // buffer free
    if (ctx->buf != NULL)
        free(ctx->buf);

    // USB close
    if (ctx->usbInitialized)
    {
        ret = ftdi_usb_close(ctx->ftdi);
        if (ret < 0)
        {
            fprintf(stderr, "FTDI: ftdi_usb_close - fail.\n");
            fprintf(stderr, "FTDI: ret=%d (%s).\n", ret, ftdi_get_error_string(ctx->ftdi));
        }
        ctx->usbInitialized = false; // maybe not needed
    }

    // free FTDI context
    if (ctx->ftdi != NULL)
        ftdi_free(ctx->ftdi);
}