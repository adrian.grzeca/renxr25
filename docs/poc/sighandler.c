#include <signal.h>
#include <stdbool.h>
#include <unistd.h>

#include "sighandler.h"

size_t sigSafeStrLen(const char *str)
{
    int len;
    for (len = 0; str[len] != '\0' && len < STRLEN_LIMIT; len++)
        ;
    return len;
}

void signalHandler(int sigNum)
{
    switch (sigNum)
    {
    case SIGINT:
        write(STDERR_FILENO, EXIT_MSG, sigSafeStrLen(EXIT_MSG));
            exitFlag = true;
        break;
    default:
        break;
    }
}