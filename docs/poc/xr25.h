#ifndef __XR25_H__
#define __XR25_H__

#include <stdbool.h>
#include <ftdi.h>

#define ISO_BAUDRATE_SLOW 2400
#define ISO_BAUDRATE_FAST 10400
#define ISO_BUFSIZE 512 // ftdi internal fifo is only 512B

#define SAFETY_COUNTER_LOW 64

#define ISO_HIGH true
#define ISO_LOW false

typedef struct xr25Context
{
    struct ftdi_context *ftdi;
    bool usbInitialized;
    unsigned char *buf;
} xr25Context;

int xr25Init(xr25Context *ctx, int baudrate);
int xr25SetISOState(xr25Context *ctx, bool state);
int xr25ConnectToAddress(xr25Context *ctx, int address, int *calculatorId);
const char *xr25ComputerIDToStr(int calculatorId);
void xr25DeInit(xr25Context *ctx);

#endif //__XR25_H__