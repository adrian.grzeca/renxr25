#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <unistd.h>
#include <ftdi.h>

#include "xr25.h"
#include "sighandler.h"

volatile bool exitFlag;

const unsigned char syp2packet[] = {0x03, 0x31, 0x50, 0x84};
const unsigned char syp2packetLang[] = {0x04, 0x40, 0x01, 0x02, 0x47};

void die(const char *msg, xr25Context *ctx)
{
    fprintf(stderr, "EAPP: %s\nExiting..\n", msg);
    if (ctx != NULL)
    {
        xr25DeInit(ctx);
        free(ctx);
    }
    exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
    // variables
    xr25Context *xr25Ctx;
    int ret;
    int calculatorId;
    int i;
    bool langChanged;

    // assign
    xr25Ctx = NULL;
    exitFlag = false;
    langChanged = false;

    // hello
    fprintf(stderr, "Starting RenXR25..\n");
    fprintf(stderr, "v0.0.0 (PoC)\n\n");

    // signal install
    if (signal(SIGINT, signalHandler) == SIG_ERR)
        die("signal - fail.", xr25Ctx);

    // initialization
    xr25Ctx = malloc(sizeof(xr25Ctx));
    if (xr25Init(xr25Ctx, ISO_BAUDRATE_FAST) != EXIT_SUCCESS)
        die("xr25Init - fail.", xr25Ctx);

    // connect to voice synthesizer
    if (xr25ConnectToAddress(xr25Ctx, 0x52, &calculatorId) != EXIT_SUCCESS)
        die("xr25ConnectToAddress - fail.", xr25Ctx);

    // place for tests

    // testing loop
    while (!exitFlag)
    {
        // io
        fprintf(stderr, "=================================\n");

        // send some data
        ret = ftdi_write_data(xr25Ctx->ftdi, syp2packet, 4); // write sample request
        if (ret != 4)
            die("ftdi_write_data - fail.", xr25Ctx);
        usleep(25 * 1000);                  // small sleep
        ret = ftdi_tciflush(xr25Ctx->ftdi); // flush echos
        if (ret < 0)
            die("ftdi_tciflush - fail.", xr25Ctx);
        usleep(500 * 1000); // wait for response

        // read response
        ret = ftdi_read_data(xr25Ctx->ftdi, xr25Ctx->buf, ISO_BUFSIZE);
        if (ret < 0)
            die("ftdi_read_data - fail.", xr25Ctx);
        else if (ret == 0)
            fprintf(stderr, "No data.. Kline error?\n");
        else
        {
            if (xr25Ctx->buf[0] + 1 != ret)
                fprintf(stderr, "Data length mismatch! Error!\n");
        }

        // show data..
        // language
        switch (xr25Ctx->buf[3] & 0x0F)
        {
        case 0x01:
            fprintf(stderr, "Lang = French\n");
            break;
        case 0x03:
            fprintf(stderr, "Lang = English\n");
            break;
        case 0x05:
            fprintf(stderr, "Lang = German\n");
            break;
        case 0x07:
            fprintf(stderr, "Lang = Italian\n");
            break;
        case 0x09:
            fprintf(stderr, "Lang = Spanish\n");
            break;
        case 0x0B:
            fprintf(stderr, "Lang = Dutch\n");
            break;
        }
        // rep key
        fprintf(stderr, "Repkey = %X\n", (xr25Ctx->buf[5] & 0x02) >> 1);

        // sleep
        usleep(1000 * 1000); // 1000ms

        // Change language
        if (!langChanged)
        {
            langChanged = true;

            // send some data
            ret = ftdi_write_data(xr25Ctx->ftdi, syp2packetLang, 5); // write sample request
            if (ret != 5)
                die("ftdi_write_data - fail.", xr25Ctx);
            usleep(25 * 1000);                  // small sleep
            ret = ftdi_tciflush(xr25Ctx->ftdi); // flush echos
            if (ret < 0)
                die("ftdi_tciflush - fail.", xr25Ctx);
            usleep(500 * 1000); // wait for response
        }
    }

    // goodbye
    fprintf(stderr, "Success! Exiting..\n");
    if (xr25Ctx != NULL)
    {
        xr25DeInit(xr25Ctx);
        free(xr25Ctx);
    }
    return EXIT_SUCCESS;
}