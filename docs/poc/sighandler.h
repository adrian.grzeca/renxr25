#ifndef __SIGHANDLER_H__
#define __SIGHANDLER_H__

#include <signal.h>
#include <stdbool.h>

#define STRLEN_LIMIT 128
#define EXIT_MSG "\nSignal received, exiting!\n"

extern volatile bool exitFlag;

size_t sigSafeStrLen(const char* str);
void signalHandler(int sigNum);

#endif //__SIGHANDLER_H__