# renXR25

To do project description. Whole Readme is to do..

# Supported platforms

Ubuntu only; Targeted: Windows + Ubuntu.

# Building

## Build dependencies

To do list dependencies.

## Runtime dependencies

To do list.

## Building - Debian/Ubuntu

1. Create build directory `mkdir build` in root folder of project.
2. Go into that directory: `cd build`.
3. Run cmake: `cmake ..`.
4. Build using make: `make -j <number of cores>`.
5. Done.

# Development

I recommend using VSCode as IDE on Windows and Debian/Ubuntu.

## Updating .pot and .po files

1. Create build directory `mkdir build` in root folder of project.
2. Go into that directory: `cd build`.
3. Run cmake: `cmake ..`.
4. Regenerate .pot files (template for languages): `make _gen_pot`. 
5. Merge .po language files with current .pot template.  You must do to each language!
   1. For en_US (English US): `make _gen_po_en_US.UTF-8`. 
   2. For pl_PL (Polish PL): `make _gen_po_pl_PL.UTF-8`. 