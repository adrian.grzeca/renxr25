#ifndef __INTERFACESELECTWINDOW__
#define __INTERFACESELECTWINDOW__

#include <gtk/gtk.h>

#include "application.h"

typedef struct InterfaceSelectionContext
{
    ApplicationContext *application_context;
    GtkWindow *window_interface_select;
    GtkSingleSelection *selectionmodel;
} InterfaceSelectionContext;

// Declare stupid interface GObject class
#define RX25_TYPE_INTERFACE (rx25_interface_get_type())
G_DECLARE_FINAL_TYPE(RX25Interface, rx25_interface, RX25, INTERFACE, GObject)
int rx25_interface_get_bus(RX25Interface *self);
int rx25_interface_get_port(RX25Interface *self);
const char *rx25_interface_get_name(RX25Interface *self);
RX25Interface *rx25_interface_new(const char *name, int bus, int port);

void interfaceselectwindow_show(ApplicationContext *context);
void interfaceselectwindow_destroy_signal(GtkWidget *self, gpointer user_data);
void interfaceselectwindow_cancel_signal(GtkButton *button, gpointer user_data);
void interfaceselectwindow_ok_signal(GtkButton *button, gpointer user_data);
void interfaceselectwindow_factory_setup_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data);
void interfaceselectwindow_factory_bind_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data);

#endif //__INTERFACESELECTWINDOW__
