#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "xr25/xr25.h"
#include "diagnosticcardselectwindow.h"
#include "application.h"
#include "parameterobject.h"
#include "config.h"

// At this moment it is same window as interface select window. But it need refactor to ColumnView!
// Will be done in feature!!

// Declare stupid interface GObject class
struct _GXR25Diagnosticcard
{
    GObject parent;
    char name[STRING_BUFFER_SIZE];
    const XR25DiagnosticCard *card;
};

G_DEFINE_TYPE(GXR25Diagnosticcard, gxr25_diagnosticcard, G_TYPE_OBJECT)

static void
gxr25_diagnosticcard_class_init(GXR25DiagnosticcardClass *class)
{
}

static void
gxr25_diagnosticcard_init(GXR25Diagnosticcard *self)
{
}

const XR25DiagnosticCard *gxr25_diagnosticcard_get_card(GXR25Diagnosticcard *self)
{
    g_return_val_if_fail(GXR25_IS_DIAGNOSTICCARD(self), NULL);
    return self->card;
}

const char *gxr25_diagnosticcard_get_name(GXR25Diagnosticcard *self)
{
    g_return_val_if_fail(GXR25_IS_DIAGNOSTICCARD(self), NULL);
    return self->name;
}

GXR25Diagnosticcard *gxr25_diagnosticcard_new(const char *name, const XR25DiagnosticCard *card)
{
    GXR25Diagnosticcard *self;

    self = g_object_new(GXR25_TYPE_DIAGNOSTICCARD, NULL);
    strcpy(self->name, name);
    self->card = card;

    return self;
}

void diagnosticcardselectwindow_show(ApplicationContext *context)
{
    // Variables
    char buffer[STRING_BUFFER_SIZE];
    int i;
    GtkWindow *window_diagnostic_card_select;
    GtkBox *box_main, *box_button;
    GtkLabel *label_information1, *label_information2;
    GtkScrolledWindow *scrolledwindow;
    GtkListView *listview;
    GtkListItemFactory *listitemfactory;
    GtkSingleSelection *selectionmodel;
    GListStore *liststore_diagnostic_card_list;
    GtkButton *button_ok, *button_cancel;
    const XR25DiagnosticCard **diagnostic_card_list;
    DiagnosticCardSelectionContext *selection_context;

    // Populate list
    liststore_diagnostic_card_list = g_list_store_new(GXR25_TYPE_DIAGNOSTICCARD);
    diagnostic_card_list = xr25_get_diagnostic_card_list();
    for (i = 0; diagnostic_card_list[i] != NULL; i++)
    {
        g_snprintf(buffer, STRING_BUFFER_SIZE, "[%s] - %s %s - %s",
                   diagnostic_card_list[i]->code,
                   xr25_get_car_manufacturer_string(diagnostic_card_list[i]->car_manufacturer),
                   xr25_get_car_model_string(diagnostic_card_list[i]->car_manufacturer, diagnostic_card_list[i]->car_model),
                   xr25_get_computer_type_string(diagnostic_card_list[i]->type));
        g_list_store_append(liststore_diagnostic_card_list, gxr25_diagnosticcard_new(buffer, diagnostic_card_list[i]));
    }
    free(diagnostic_card_list);

    // Create window
    window_diagnostic_card_select = GTK_WINDOW(gtk_window_new());
    gtk_window_set_transient_for(window_diagnostic_card_select, GTK_WINDOW(context->window_main));
    gtk_window_set_modal(window_diagnostic_card_select, TRUE);
    gtk_window_set_resizable(window_diagnostic_card_select, FALSE);
    gtk_window_set_default_size(window_diagnostic_card_select, 600, 500);
    gtk_window_set_title(window_diagnostic_card_select, _("title_diagnostic_card_select"));

    // Create box
    box_main = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_main), 5);
    gtk_window_set_child(window_diagnostic_card_select, GTK_WIDGET(box_main));

    // Create label information #1
    label_information1 = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<span size=\"x-large\"><b>%s</b></span>", _("label_please_select_diagnostic_card"));
    gtk_label_set_markup(label_information1, buffer);
    gtk_box_append(box_main, GTK_WIDGET(label_information1));

    // Create label information #2
    label_information2 = GTK_LABEL(gtk_label_new(NULL));
    gtk_label_set_wrap(label_information2, TRUE);
    gtk_label_set_justify(label_information2, GTK_JUSTIFY_CENTER);
    gtk_label_set_markup(label_information2, _("label_please_select_diagnostic_card_note"));
    gtk_box_append(box_main, GTK_WIDGET(label_information2));

    // Create scrolled window
    scrolledwindow = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new());
    gtk_widget_set_hexpand(GTK_WIDGET(scrolledwindow), TRUE);
    gtk_widget_set_vexpand(GTK_WIDGET(scrolledwindow), TRUE);
    gtk_widget_set_size_request(GTK_WIDGET(scrolledwindow), -1, 300);
    gtk_box_append(box_main, GTK_WIDGET(scrolledwindow));

    // Create list view
    selectionmodel = gtk_single_selection_new(G_LIST_MODEL(liststore_diagnostic_card_list));
    listitemfactory = gtk_signal_list_item_factory_new();
    g_signal_connect(listitemfactory, "setup", G_CALLBACK(diagnosticcardselectwindow_factory_setup_signal), NULL);
    g_signal_connect(listitemfactory, "bind", G_CALLBACK(diagnosticcardselectwindow_factory_bind_signal), NULL);
    listview = GTK_LIST_VIEW(gtk_list_view_new(GTK_SELECTION_MODEL(selectionmodel), listitemfactory));
    gtk_scrolled_window_set_child(scrolledwindow, GTK_WIDGET(listview));

    // Create box for buttons
    box_button = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_button), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_button), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_button), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_button), 5);
    gtk_box_append(box_main, GTK_WIDGET(box_button));

    // Create selection context
    selection_context = malloc(sizeof(DiagnosticCardSelectionContext));
    selection_context->application_context = context;
    selection_context->window_diagnostic_card_select = window_diagnostic_card_select;
    selection_context->selectionmodel = selectionmodel;

    // Create button ok
    button_ok = GTK_BUTTON(gtk_button_new());
    gtk_button_set_label(button_ok, _("button_ok"));
    gtk_widget_set_hexpand(GTK_WIDGET(button_ok), TRUE);
    gtk_box_append(box_button, GTK_WIDGET(button_ok));
    g_signal_connect(button_ok, "clicked", G_CALLBACK(diagnosticcardselectwindow_ok_signal), selection_context);

    // Create button cancel
    button_cancel = GTK_BUTTON(gtk_button_new());
    gtk_button_set_label(button_cancel, _("button_cancel"));
    gtk_widget_set_hexpand(GTK_WIDGET(button_cancel), TRUE);
    gtk_box_append(box_button, GTK_WIDGET(button_cancel));
    g_signal_connect(button_cancel, "clicked", G_CALLBACK(diagnosticcardselectwindow_cancel_signal), window_diagnostic_card_select);

    // Show window
    g_signal_connect(button_cancel, "destroy", G_CALLBACK(diagnosticcardselectwindow_destroy_signal), selection_context);
    gtk_widget_show(GTK_WIDGET(window_diagnostic_card_select));
}

void diagnosticcardselectwindow_destroy_signal(GtkWidget *self, gpointer user_data)
{
    // Destroys selection context
    free(user_data);
}

void diagnosticcardselectwindow_cancel_signal(GtkButton *button, gpointer user_data)
{
    GtkWindow *window_diagnostic_card_select;
    window_diagnostic_card_select = GTK_WINDOW(user_data);
    gtk_window_destroy(window_diagnostic_card_select);
}

void diagnosticcardselectwindow_ok_signal(GtkButton *button, gpointer user_data)
{
    // Variables
    DiagnosticCardSelectionContext *selection_context;
    ApplicationContext *application_context;
    GtkWindow *window_diagnostic_card_select;
    GtkSingleSelection *selection_model;
    GXR25Diagnosticcard *object;
    GtkMessageDialog *dialog_box;
    const char *string;
    char buffer[STRING_BUFFER_SIZE];
    int i;

    // Copy pointers from temporary context
    selection_context = (DiagnosticCardSelectionContext *)user_data;
    application_context = selection_context->application_context;
    window_diagnostic_card_select = selection_context->window_diagnostic_card_select;
    selection_model = selection_context->selectionmodel;
    // free(selection_context);

    // Get selected diagnostic card
    object = GXR25_DIAGNOSTICCARD(gtk_single_selection_get_selected_item(selection_model));
    string = gxr25_diagnosticcard_get_name(object);

    // Set diagnostic card and close window
    application_context->diagnostic_card = gxr25_diagnosticcard_get_card(object);
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<b>%s</b>", string);
    gtk_label_set_markup(application_context->label_diagnostic_card, buffer);
    gtk_label_set_text(application_context->label_cable_type, xr25_get_cable_type_string(application_context->diagnostic_card->cable_type));
    gtk_window_destroy(window_diagnostic_card_select);

    // Log to console
    g_critical("Selected diagnostic card UNIQUE CODE=%s STRING=%s",
               application_context->diagnostic_card->unique_code,
               string);

    // Setup main window
    // Setup parameters
    g_list_store_remove_all(application_context->liststore_parameter);
    for (i = 0; application_context->diagnostic_card->parameter_list[i] != NULL; i++)
    {
        g_list_store_append(application_context->liststore_parameter, gxr25_parameter_new(
            application_context->diagnostic_card->parameter_list[i]->label,
            "",
            application_context->diagnostic_card->parameter_list[i]->unit
        ));
    }

    // Show message box
    dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(application_context->window_main,
                                                           GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                           GTK_MESSAGE_INFO,
                                                           GTK_BUTTONS_OK,
                                                           _("success_error")));
    gtk_message_dialog_format_secondary_text(dialog_box, "%s: %s", _("selected_diagnostic_card_info"), string);
    g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
    gtk_window_present(GTK_WINDOW(dialog_box));
}

void diagnosticcardselectwindow_factory_setup_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
    GtkWidget *label;
    label = gtk_label_new(NULL);
    gtk_list_item_set_child(list_item, label);
}

void diagnosticcardselectwindow_factory_bind_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
    GtkWidget *label;
    GXR25Diagnosticcard *object;
    label = gtk_list_item_get_child(list_item);
    object = gtk_list_item_get_item(list_item);
    gtk_label_set_text(GTK_LABEL(label), gxr25_diagnosticcard_get_name(object));
}