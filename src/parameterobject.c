#include <gtk/gtk.h>

#include "parameterobject.h"

struct _GXR25Parameter
{
    GObject parent;
    char name[PARAMETER_STRING_MAX_LENGTH];
    char value[PARAMETER_STRING_MAX_LENGTH];
    char unit[PARAMETER_STRING_MAX_LENGTH];
};

G_DEFINE_TYPE(GXR25Parameter, gxr25_parameter, G_TYPE_OBJECT)

static void
gxr25_parameter_class_init(GXR25ParameterClass *class)
{
}

static void
gxr25_parameter_init(GXR25Parameter *self)
{
}

const char *gxr25_parameter_get_name(GXR25Parameter *self)
{
    g_return_val_if_fail(GXR25_IS_PARAMETER(self), NULL);
    return self->name;
}

const char *gxr25_parameter_get_value(GXR25Parameter *self)
{
    g_return_val_if_fail(GXR25_IS_PARAMETER(self), NULL);
    return self->value;
}

void gxr25_parameter_set_value(GXR25Parameter *self, const char *value)
{
    g_return_if_fail(GXR25_IS_PARAMETER(self));
    strncpy(self->value, value, PARAMETER_STRING_MAX_LENGTH);
}

const char *gxr25_parameter_get_unit(GXR25Parameter *self)
{
    g_return_val_if_fail(GXR25_IS_PARAMETER(self), NULL);
    return self->unit;
}

GXR25Parameter *gxr25_parameter_new(const char *name, const char *value, const char *unit)
{
    GXR25Parameter *self;

    self = g_object_new(RX25_TYPE_PARAMETER, NULL);
    strncpy(self->name, name, PARAMETER_STRING_MAX_LENGTH);
    strncpy(self->value, value, PARAMETER_STRING_MAX_LENGTH);
    strncpy(self->unit, unit, PARAMETER_STRING_MAX_LENGTH);

    return self;
}