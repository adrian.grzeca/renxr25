#ifndef __PARAMETEROBECT__
#define __PARAMETEROBECT__

#include <gtk/gtk.h>

#define PARAMETER_STRING_MAX_LENGTH 64

#define PARAMETER_GPOINTER_TYPE_NAME ((void *)1)
#define PARAMETER_GPOINTER_TYPE_VALUE ((void *)2)
#define PARAMETER_GPOINTER_TYPE_UNIT ((void *)3)

#define RX25_TYPE_PARAMETER (gxr25_parameter_get_type())
G_DECLARE_FINAL_TYPE(GXR25Parameter, gxr25_parameter, GXR25, PARAMETER, GObject)
const char *gxr25_parameter_get_name(GXR25Parameter *self);
const char *gxr25_parameter_get_value(GXR25Parameter *self);
void gxr25_parameter_set_value(GXR25Parameter *self, const char *value);
const char *gxr25_parameter_get_unit(GXR25Parameter *self);
GXR25Parameter *gxr25_parameter_new(const char *name, const char *value, const char *unit);

#endif //__PARAMETEROBECT__
