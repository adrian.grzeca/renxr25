#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <ftdi.h>

#include "aboutwindow.h"
#include "application.h"
#include "config.h"

void aboutwindow_show(ApplicationContext *context)
{
    // Variables
    char buffer[STRING_BUFFER_SIZE];
    struct ftdi_version_info ftdi_version;
    GtkWindow *window_about;
    GtkBox *box_main;
    GtkImage *image_logo;
    GtkSeparator *separator;
    GtkLabel *label_app_name, *label_author, *label_website, *label_ftdi, *label_license1, *label_license2;

    // Create window
    window_about = GTK_WINDOW(gtk_window_new());
    gtk_window_set_transient_for(window_about, GTK_WINDOW(context->window_main));
    gtk_window_set_modal(window_about, TRUE);
    gtk_window_set_resizable(window_about, FALSE);
    gtk_window_set_title(window_about, _("title_about_box"));

    // Create box
    box_main = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_main), 5);
    gtk_window_set_child(window_about, GTK_WIDGET(box_main));

    // Create logo
    image_logo = GTK_IMAGE(gtk_image_new());
    gtk_image_set_from_icon_name(image_logo, "renxr25-icon");
    gtk_image_set_pixel_size(image_logo, 128);
    gtk_box_append(box_main, GTK_WIDGET(image_logo));

    // Create app name
    label_app_name = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<span size=\"x-large\"><b>%s</b></span>", APP_VERNAME_STRING);
    gtk_label_set_markup(label_app_name, buffer);
    gtk_box_append(box_main, GTK_WIDGET(label_app_name));

    // Create author
    label_author = GTK_LABEL(gtk_label_new("© 2023 Adrian Grzeca"));
    gtk_box_append(box_main, GTK_WIDGET(label_author));

    // Create website
    label_website = GTK_LABEL(gtk_label_new(NULL));
    gtk_label_set_markup(label_website, "<a href=\"https://gitlab.com/adrian.grzeca\">https://gitlab.com/adrian.grzeca</a>");
    gtk_box_append(box_main, GTK_WIDGET(label_website));

    // Create label license1
    label_license1 = GTK_LABEL(gtk_label_new(_("label_license_gplv3")));
    gtk_label_set_wrap(label_license1, TRUE);
    gtk_label_set_max_width_chars(label_license1, 40);
    gtk_label_set_justify(label_license1, GTK_JUSTIFY_CENTER);
    gtk_box_append(box_main, GTK_WIDGET(label_license1));

    // Create label license2
    label_license2 = GTK_LABEL(gtk_label_new(_("label_license_rust_is_the_worst_programing_language")));
    gtk_label_set_wrap(label_license2, TRUE);
    gtk_label_set_max_width_chars(label_license2, 40);
    gtk_label_set_justify(label_license2, GTK_JUSTIFY_CENTER);
    gtk_box_append(box_main, GTK_WIDGET(label_license2));

    // Create separator
    separator = GTK_SEPARATOR(gtk_separator_new(GTK_ORIENTATION_HORIZONTAL));
    gtk_box_append(box_main, GTK_WIDGET(separator));

    // Create FTDI version
    label_ftdi = GTK_LABEL(gtk_label_new(NULL));
    ftdi_version = ftdi_get_library_version();
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<b>%s:</b> %s", _("label_ftdi_version"), ftdi_version.version_str);
    gtk_label_set_markup(label_ftdi, buffer);
    gtk_box_append(box_main, GTK_WIDGET(label_ftdi));

    // Show window
    gtk_widget_show(GTK_WIDGET(window_about));
}