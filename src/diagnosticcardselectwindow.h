#ifndef __DIAGNOSTICCARDSELECTWINDOW__
#define __DIAGNOSTICCARDSELECTWINDOW__

#include <gtk/gtk.h>

#include "application.h"
#include "xr25/xr25.h"

typedef struct DiagnosticCardSelectionContext
{
    ApplicationContext *application_context;
    GtkWindow *window_diagnostic_card_select;
    GtkSingleSelection *selectionmodel;
} DiagnosticCardSelectionContext;

// Declare stupid diagnostic card GObject class
#define GXR25_TYPE_DIAGNOSTICCARD (gxr25_diagnosticcard_get_type())
G_DECLARE_FINAL_TYPE(GXR25Diagnosticcard, gxr25_diagnosticcard, GXR25, DIAGNOSTICCARD, GObject)
const XR25DiagnosticCard *gxr25_diagnosticcard_get_card(GXR25Diagnosticcard *self);
const char *gxr25_diagnosticcard_get_name(GXR25Diagnosticcard *self);
GXR25Diagnosticcard *gxr25_diagnosticcard_new(const char *name, const XR25DiagnosticCard *card);

void diagnosticcardselectwindow_show(ApplicationContext *context);
void diagnosticcardselectwindow_destroy_signal(GtkWidget *self, gpointer user_data);
void diagnosticcardselectwindow_cancel_signal(GtkButton *button, gpointer user_data);
void diagnosticcardselectwindow_ok_signal(GtkButton *button, gpointer user_data);
void diagnosticcardselectwindow_factory_setup_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data);
void diagnosticcardselectwindow_factory_bind_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data);

#endif //__DIAGNOSTICCARDSELECTWINDOW__