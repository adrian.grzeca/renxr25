#ifndef __APPLICATION__
#define __APPLICATION__

#include <gtk/gtk.h>
#include <ftdi.h>
#include <pthread.h>

#include "xr25/xr25.h"

typedef enum DiagnosticState {
    DiagnosticStateNo,
    DiagnosticStateYes
} DiagnosticState;

typedef struct ApplicationContext
{
    GApplication *application;
    GtkWindow *window_main;
    GtkButton *button_interface_select, *button_diagnostic_card_select, *button_startstop_diag;
    GtkLabel *label_interface_name, *label_cable_type, *label_diagnostic_card;
    GListStore *liststore_parameter;
    struct ftdi_context *ftdi;
    const XR25DiagnosticCard *diagnostic_card;
    int interface_bus, interface_port;
    DiagnosticState diagnostic_state;
    const XR25DiagnosticContext *diagnostic_context;
    guint timeout_id;
    unsigned long last_update;
} ApplicationContext;

void application_init(ApplicationContext *context);
void application_deinit(ApplicationContext *context);
void application_activate(GApplication *application, ApplicationContext *context);
void application_show_about_window(GSimpleAction *action, GVariant *parameter, gpointer user_data);
void application_exit_action(GSimpleAction *action, GVariant *parameter, gpointer user_data);
void application_exit_dialog_signal(GtkDialog *dialog, gint response_id, gpointer user_data);
void application_open_select_interface_window_signal(GtkButton *button, gpointer user_data);
void application_open_select_diagnostic_card_signal(GtkButton *button, gpointer user_data);
void application_parameters_factory_setup_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data);
void application_parameters_factory_bind_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data);
void application_startstop_diag_signal(GtkButton *button, gpointer user_data);
gboolean application_refresh_timeout(gpointer user_data);
void application_lock_ui(ApplicationContext *context);
void application_unlock_ui(ApplicationContext *context);

#endif //__APPLICATION__