#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <ftdi.h>
#include <pthread.h>

#include "application.h"
#include "aboutwindow.h"
#include "interfaceselectwindow.h"
#include "diagnosticcardselectwindow.h"
#include "parameterobject.h"
#include "xr25/xr25.h"
#include "config.h"

void application_init(ApplicationContext *context)
{
    // Initialize context
    context->interface_bus = -1;
    context->interface_port = -1;
    context->diagnostic_card = NULL;
    context->diagnostic_context = NULL;
    context->diagnostic_state = DiagnosticStateNo;
    context->timeout_id = 0;
    context->last_update = 0;
}

void application_deinit(ApplicationContext *context)
{
}

void application_activate(GApplication *application, ApplicationContext *context)
{
    // Temporary variables
    char buffer[STRING_BUFFER_SIZE];
    GtkMessageDialog *dialog_ftdi_error;
    GtkIconTheme *icontheme;
    GtkBox *box_main, *box_topbar, *box_diagnostic_card, *box_data, *box_faults, *box_parameters, *box_control;
    GtkScrolledWindow *scrolledwindow_faults, *scrolledwindow_parameters, *scrolledwindow_control;
    GtkSeparator *separator_topbar, *separator_topbar2, *separator_diagnostic_card, *separator_data1, *separator_data2;
    GtkLabel *label_interface, *label_cable, *label_faults, *label_parameters, *label_control;
    GMenu *menu_bar, *submenu_file, *submenu_help;
    GMenuItem *menu_bar_item_file, *submenu_file_item_quit, *menu_bar_item_help, *submenu_help_item_about;
    GSimpleAction *action_quit, *action_about;
    GtkNoSelection *selectionmodel_parameters;
    GtkColumnView *columnview_parameters;
    GtkColumnViewColumn *columnview_parameters_name, *columnview_parameters_value, *columnview_parameters_unit;
    GtkListItemFactory *listitemfactory_parameters_name, *listitemfactory_parameters_value, *listitemfactory_parameters_unit;

    // Set default icon
    icontheme = gtk_icon_theme_get_for_display(gdk_display_get_default());
    gtk_icon_theme_add_resource_path(icontheme, "/me/grzeca/renxr25/icons");
    if (gtk_icon_theme_has_icon(icontheme, "renxr25-icon") != 1)
    {
        g_critical("No icon found");
    }
    gtk_window_set_default_icon_name("renxr25-icon");

    // Create application window
    context->application = application;
    context->window_main = GTK_WINDOW(gtk_application_window_new(GTK_APPLICATION(context->application)));

    // Set up actions for menu bar
    action_quit = g_simple_action_new("quit", NULL);
    g_action_map_add_action(G_ACTION_MAP(application), G_ACTION(action_quit));
    g_signal_connect(action_quit, "activate", G_CALLBACK(application_exit_action), context);
    action_about = g_simple_action_new("about", NULL);
    g_action_map_add_action(G_ACTION_MAP(application), G_ACTION(action_about));
    g_signal_connect(action_about, "activate", G_CALLBACK(application_show_about_window), context);

    // Build menu bar
    menu_bar = g_menu_new();
    menu_bar_item_file = g_menu_item_new(_("submenu_file"), NULL);
    submenu_file = g_menu_new();
    submenu_file_item_quit = g_menu_item_new(_("submenu_file_quit"), "app.quit");
    menu_bar_item_help = g_menu_item_new(_("submenu_help"), NULL);
    submenu_help = g_menu_new();
    submenu_help_item_about = g_menu_item_new(_("submenu_help_about"), "app.about");
    g_menu_append_item(submenu_file, submenu_file_item_quit);
    g_menu_item_set_submenu(menu_bar_item_file, G_MENU_MODEL(submenu_file));
    g_menu_append_item(submenu_help, submenu_help_item_about);
    g_menu_item_set_submenu(menu_bar_item_help, G_MENU_MODEL(submenu_help));
    g_menu_append_item(menu_bar, menu_bar_item_file);
    g_menu_append_item(menu_bar, menu_bar_item_help);

    // Setup menu bar
    gtk_application_set_menubar(GTK_APPLICATION(context->application), G_MENU_MODEL(menu_bar));
    gtk_application_window_set_show_menubar(GTK_APPLICATION_WINDOW(context->window_main), TRUE);

    // Setup main box
    box_main = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_main), 5);
    gtk_window_set_child(context->window_main, GTK_WIDGET(box_main));

    // Setup topbar box
    box_topbar = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_topbar), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_topbar), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_topbar), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_topbar), 5);
    gtk_box_append(box_main, GTK_WIDGET(box_topbar));

    // Setup button for selecting interface
    context->button_interface_select = GTK_BUTTON(gtk_button_new());
    gtk_button_set_label(context->button_interface_select, _("button_select_interface"));
    gtk_box_append(box_topbar, GTK_WIDGET(context->button_interface_select));
    g_signal_connect(context->button_interface_select, "clicked", G_CALLBACK(application_open_select_interface_window_signal), context);

    // Setup interface label
    label_interface = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<b>%s:</b>", _("label_interface"));
    gtk_label_set_markup(label_interface, buffer);
    gtk_box_append(box_topbar, GTK_WIDGET(label_interface));

    // Setup interface label name
    context->label_interface_name = GTK_LABEL(gtk_label_new(NULL));
    gtk_widget_set_hexpand(GTK_WIDGET(context->label_interface_name), TRUE);
    gtk_widget_set_halign(GTK_WIDGET(context->label_interface_name), GTK_ALIGN_START);
    gtk_box_append(box_topbar, GTK_WIDGET(context->label_interface_name));

    // Setup topbar2 separator
    separator_topbar2 = GTK_SEPARATOR(gtk_separator_new(GTK_ORIENTATION_VERTICAL));
    gtk_box_append(box_topbar, GTK_WIDGET(separator_topbar2));

    // Setup cabel label
    label_cable = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<b>%s:</b>", _("label_cable"));
    gtk_label_set_markup(label_cable, buffer);
    gtk_box_append(box_topbar, GTK_WIDGET(label_cable));

    // Setup cabel label name
    context->label_cable_type = GTK_LABEL(gtk_label_new(NULL));
    gtk_widget_set_size_request(GTK_WIDGET(context->label_cable_type), 100, -1);
    gtk_widget_set_halign(GTK_WIDGET(context->label_cable_type), GTK_ALIGN_START);
    gtk_box_append(box_topbar, GTK_WIDGET(context->label_cable_type));

    // Setup topbar separator
    separator_topbar = GTK_SEPARATOR(gtk_separator_new(GTK_ORIENTATION_HORIZONTAL));
    gtk_box_append(box_main, GTK_WIDGET(separator_topbar));

    // Setup diagnostic card box
    box_diagnostic_card = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_diagnostic_card), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_diagnostic_card), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_diagnostic_card), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_diagnostic_card), 5);
    gtk_box_append(box_main, GTK_WIDGET(box_diagnostic_card));

    // Setup diagnostic card select button
    context->button_diagnostic_card_select = GTK_BUTTON(gtk_button_new());
    gtk_button_set_label(context->button_diagnostic_card_select, _("button_select_diagnostic_card"));
    gtk_box_append(box_diagnostic_card, GTK_WIDGET(context->button_diagnostic_card_select));
    g_signal_connect(context->button_diagnostic_card_select, "clicked", G_CALLBACK(application_open_select_diagnostic_card_signal), context);

    // Setup diagnostic card label
    context->label_diagnostic_card = GTK_LABEL(gtk_label_new(NULL));
    gtk_widget_set_hexpand(GTK_WIDGET(context->label_diagnostic_card), TRUE);
    gtk_box_append(box_diagnostic_card, GTK_WIDGET(context->label_diagnostic_card));

    // Setup start/stop diagnostic button (G13*)
    context->button_startstop_diag = GTK_BUTTON(gtk_button_new());
    gtk_button_set_label(context->button_startstop_diag, _("button_start_diag"));
    gtk_box_append(box_diagnostic_card, GTK_WIDGET(context->button_startstop_diag));
    g_signal_connect(context->button_startstop_diag, "clicked", G_CALLBACK(application_startstop_diag_signal), context);

    // Setup diagnostic_card separator
    separator_diagnostic_card = GTK_SEPARATOR(gtk_separator_new(GTK_ORIENTATION_HORIZONTAL));
    gtk_box_append(box_main, GTK_WIDGET(separator_diagnostic_card));

    // Setup data box
    box_data = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_data), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_data), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_data), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_data), 5);
    gtk_box_append(box_main, GTK_WIDGET(box_data));

    // Setup faults box
    box_faults = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_faults), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_faults), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_faults), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_faults), 5);
    gtk_box_append(box_data, GTK_WIDGET(box_faults));

    // Setup faults label
    label_faults = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<b>%s</b>", _("label_faults"));
    gtk_label_set_markup(label_faults, buffer);
    gtk_box_append(box_faults, GTK_WIDGET(label_faults));

    // Setup faults scrolled window
    scrolledwindow_faults = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new());
    // gtk_widget_set_hexpand(GTK_WIDGET(scrolledwindow_faults), TRUE);
    gtk_widget_set_vexpand(GTK_WIDGET(scrolledwindow_faults), TRUE);
    gtk_widget_set_size_request(GTK_WIDGET(scrolledwindow_faults), 430, -1);
    gtk_box_append(box_faults, GTK_WIDGET(scrolledwindow_faults));

    // Setup data 1 separator
    separator_data1 = GTK_SEPARATOR(gtk_separator_new(GTK_ORIENTATION_VERTICAL));
    gtk_box_append(box_data, GTK_WIDGET(separator_data1));

    // Setup parameters box
    box_parameters = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_parameters), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_parameters), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_parameters), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_parameters), 5);
    gtk_box_append(box_data, GTK_WIDGET(box_parameters));

    // Setup parameters label
    label_parameters = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<b>%s</b>", _("label_parameters"));
    gtk_label_set_markup(label_parameters, buffer);
    gtk_box_append(box_parameters, GTK_WIDGET(label_parameters));

    // Setup parameters scrolled window
    scrolledwindow_parameters = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new());
    // gtk_widget_set_hexpand(GTK_WIDGET(scrolledwindow_parameters), TRUE);
    gtk_widget_set_vexpand(GTK_WIDGET(scrolledwindow_parameters), TRUE);
    gtk_widget_set_size_request(GTK_WIDGET(scrolledwindow_parameters), 430, -1);
    gtk_box_append(box_parameters, GTK_WIDGET(scrolledwindow_parameters));

    // Create parameter list
    context->liststore_parameter = g_list_store_new(RX25_TYPE_PARAMETER);

    // Setup parameters column view
    selectionmodel_parameters = gtk_no_selection_new(G_LIST_MODEL(context->liststore_parameter));
    columnview_parameters = GTK_COLUMN_VIEW(gtk_column_view_new(GTK_SELECTION_MODEL(selectionmodel_parameters)));
    gtk_widget_set_hexpand(GTK_WIDGET(columnview_parameters), TRUE);
    gtk_scrolled_window_set_child(scrolledwindow_parameters, GTK_WIDGET(columnview_parameters));

    // Setup column name
    listitemfactory_parameters_name = gtk_signal_list_item_factory_new();
    g_signal_connect(listitemfactory_parameters_name, "setup", G_CALLBACK(application_parameters_factory_setup_signal), NULL);
    g_signal_connect(listitemfactory_parameters_name, "bind", G_CALLBACK(application_parameters_factory_bind_signal), PARAMETER_GPOINTER_TYPE_NAME);
    columnview_parameters_name = gtk_column_view_column_new(_("columtitle_name"), listitemfactory_parameters_name);
    gtk_column_view_column_set_expand(columnview_parameters_name, TRUE);
    gtk_column_view_append_column(columnview_parameters, columnview_parameters_name);

    // Setup column value
    listitemfactory_parameters_value = gtk_signal_list_item_factory_new();
    g_signal_connect(listitemfactory_parameters_value, "setup", G_CALLBACK(application_parameters_factory_setup_signal), NULL);
    g_signal_connect(listitemfactory_parameters_value, "bind", G_CALLBACK(application_parameters_factory_bind_signal), PARAMETER_GPOINTER_TYPE_VALUE);
    columnview_parameters_value = gtk_column_view_column_new(_("columtitle_value"), listitemfactory_parameters_value);
    gtk_column_view_column_set_expand(columnview_parameters_value, TRUE);
    gtk_column_view_append_column(columnview_parameters, columnview_parameters_value);

    // Setup column unit
    listitemfactory_parameters_unit = gtk_signal_list_item_factory_new();
    g_signal_connect(listitemfactory_parameters_unit, "setup", G_CALLBACK(application_parameters_factory_setup_signal), NULL);
    g_signal_connect(listitemfactory_parameters_unit, "bind", G_CALLBACK(application_parameters_factory_bind_signal), PARAMETER_GPOINTER_TYPE_UNIT);
    columnview_parameters_unit = gtk_column_view_column_new(_("columtitle_unit"), listitemfactory_parameters_unit);
    gtk_column_view_append_column(columnview_parameters, columnview_parameters_unit);

    // Setup data 2 separator
    separator_data2 = GTK_SEPARATOR(gtk_separator_new(GTK_ORIENTATION_VERTICAL));
    gtk_box_append(box_data, GTK_WIDGET(separator_data2));

    // Setup control box
    box_control = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_control), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_control), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_control), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_control), 5);
    gtk_box_append(box_data, GTK_WIDGET(box_control));

    // Setup control label
    label_control = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<b>%s</b>", _("label_control"));
    gtk_label_set_markup(label_control, buffer);
    gtk_box_append(box_control, GTK_WIDGET(label_control));

    // Setup control scrolled window
    scrolledwindow_control = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new());
    // gtk_widget_set_hexpand(GTK_WIDGET(scrolledwindow_control), TRUE);
    gtk_widget_set_vexpand(GTK_WIDGET(scrolledwindow_control), TRUE);
    gtk_widget_set_size_request(GTK_WIDGET(scrolledwindow_control), 325, -1);
    gtk_box_append(box_control, GTK_WIDGET(scrolledwindow_control));

    // Create and show window
    gtk_window_set_application(context->window_main, GTK_APPLICATION(context->application));
    gtk_window_set_title(context->window_main, APP_VERNAME_STRING);
    gtk_window_set_resizable(context->window_main, FALSE);
    gtk_window_set_default_size(context->window_main, 1300, 700);
    // gtk_window_set_interactive_debugging(TRUE);
    gtk_window_present(context->window_main);

    // Unref
    g_object_unref(menu_bar_item_file);
    g_object_unref(submenu_file);
    g_object_unref(submenu_file_item_quit);
    g_object_unref(menu_bar_item_help);
    g_object_unref(submenu_help);
    g_object_unref(submenu_help_item_about);
    g_object_unref(action_quit);
    g_object_unref(action_about);

    // Initialize FTDI
    context->ftdi = ftdi_new();
    if (context->ftdi == NULL)
    {
        // Fail - locks main window and closes after closing dialog
        dialog_ftdi_error = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                      GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                      GTK_MESSAGE_ERROR,
                                                                      GTK_BUTTONS_OK,
                                                                      _("fatal_error")));
        gtk_message_dialog_format_secondary_text(dialog_ftdi_error, _("ftdi_error_new"));
        g_signal_connect(dialog_ftdi_error, "response", G_CALLBACK(application_exit_dialog_signal), context);
        gtk_window_present(GTK_WINDOW(dialog_ftdi_error));
    }
}

void application_show_about_window(GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    aboutwindow_show((ApplicationContext *)user_data);
}

void application_exit_action(GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    application_exit_dialog_signal(NULL, 0, user_data);
}

void application_exit_dialog_signal(GtkDialog *dialog, gint response_id, gpointer user_data)
{
    ApplicationContext *context;
    context = (ApplicationContext *)user_data;
    g_application_quit(context->application);
}

void application_open_select_interface_window_signal(GtkButton *button, gpointer user_data)
{
    ApplicationContext *context;
    context = (ApplicationContext *)user_data;
    interfaceselectwindow_show(context);
}

void application_open_select_diagnostic_card_signal(GtkButton *button, gpointer user_data)
{
    ApplicationContext *context;
    context = (ApplicationContext *)user_data;
    diagnosticcardselectwindow_show(context);
}

void application_parameters_factory_setup_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
    GtkWidget *label;
    label = gtk_label_new(NULL);
    gtk_widget_set_halign(label, GTK_ALIGN_START);
    gtk_list_item_set_child(list_item, label);
}

void application_parameters_factory_bind_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
    GtkWidget *label;
    GXR25Parameter *object;
    const char *string;
    char buffer[STRING_BUFFER_SIZE];
    label = gtk_list_item_get_child(list_item);
    object = gtk_list_item_get_item(list_item);
    if (user_data == PARAMETER_GPOINTER_TYPE_NAME)
        string = gettext(gxr25_parameter_get_name(object));
    else if (user_data == PARAMETER_GPOINTER_TYPE_VALUE)
        string = gxr25_parameter_get_value(object);
    else if (user_data == PARAMETER_GPOINTER_TYPE_UNIT)
        string = gxr25_parameter_get_unit(object);
    else
        string = "??";

    g_snprintf(buffer, STRING_BUFFER_SIZE, "<small>%s</small>", string);
    gtk_label_set_markup(GTK_LABEL(label), buffer);
}

void application_startstop_diag_signal(GtkButton *button, gpointer user_data)
{
    ApplicationContext *context;
    GtkMessageDialog *dialog_box, *dialog_box_connect;
    int ret;
    gboolean ret2;

    context = (ApplicationContext *)user_data;

    if (context->diagnostic_state == DiagnosticStateNo)
    {
        // Check if interface is selected
        if (context->interface_bus == -1 && context->interface_port == -1)
        {
            // Fail - no interface selected
            dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                   GTK_MESSAGE_ERROR,
                                                                   GTK_BUTTONS_OK,
                                                                   _("warn_error")));
            gtk_message_dialog_format_secondary_text(dialog_box, _("error_no_interface_selected"));
            g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
            gtk_window_present(GTK_WINDOW(dialog_box));

            return;
        }

        // Check if card is selected
        if (context->diagnostic_card == NULL)
        {
            // Fail - no interface selected
            dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                   GTK_MESSAGE_ERROR,
                                                                   GTK_BUTTONS_OK,
                                                                   _("warn_error")));
            gtk_message_dialog_format_secondary_text(dialog_box, _("error_no_diagnostic_card_selected"));
            g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
            gtk_window_present(GTK_WINDOW(dialog_box));

            return;
        }

        // Looks fine!
        application_lock_ui(context);

        // This is SHITY work around. Window do not show acctualy! But it makes everything grey :)
        dialog_box_connect = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                       GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                       GTK_MESSAGE_ERROR,
                                                                       GTK_BUTTONS_OK,
                                                                       _("info_error")));
        gtk_message_dialog_format_secondary_text(dialog_box_connect, _("error_connecting"));
        gtk_window_present(GTK_WINDOW(dialog_box_connect));

        context->diagnostic_context = xr25_malloc_diagnostic_context(context->diagnostic_card,
                                                                     context->ftdi,
                                                                     context->interface_bus,
                                                                     context->interface_port);
        // Connect to calculator
        ret = xr25_connect((XR25DiagnosticContext *)context->diagnostic_context);
        if (ret < 0)
        {
            // Fail - cannot connect to calculator
            g_critical("Cannot connect - code: %d", ret);
            gtk_window_close(GTK_WINDOW(dialog_box_connect));
            dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                   GTK_MESSAGE_ERROR,
                                                                   GTK_BUTTONS_OK,
                                                                   _("warn_error")));
            gtk_message_dialog_format_secondary_text(dialog_box, _("error_cannot_connect_to_calculator"));
            g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
            gtk_window_present(GTK_WINDOW(dialog_box));

            xr25_free_diagnostic_context(context->diagnostic_context);
            application_unlock_ui(context);

            return;
        }

        // Create pthread
        ret = xr25_start_worker(context->diagnostic_context);
        if (ret < 0)
        {
            // Fail - cannot start worker
            g_critical("Cannot start worker - code: %d", ret);
            gtk_window_close(GTK_WINDOW(dialog_box_connect));
            dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                   GTK_MESSAGE_ERROR,
                                                                   GTK_BUTTONS_OK,
                                                                   _("warn_error")));
            gtk_message_dialog_format_secondary_text(dialog_box, _("error_cannot_connect_to_calculator"));
            g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
            gtk_window_present(GTK_WINDOW(dialog_box));

            xr25_free_diagnostic_context(context->diagnostic_context);
            application_unlock_ui(context);

            return;
        }

        // Create timer to check worker thread and update UI
        context->timeout_id = g_timeout_add(TICK_REFRESH_MSEC, application_refresh_timeout, context);

        // Close window
        gtk_window_close(GTK_WINDOW(dialog_box_connect));

        return;
    }
    else if (context->diagnostic_state == DiagnosticStateYes)
    {
        // Remove timeout
        ret2 = g_source_remove(context->timeout_id);
        if (ret2 == FALSE)
        {
            g_critical("Cannot remote timer..");
        }

        // Disconnect
        ret = xr25_disconnect((XR25DiagnosticContext *)context->diagnostic_context);
        if (ret < 0)
        {
            // Fail - cannot disconnect from calculator
            g_critical("Cannot disconnect - code: %d", ret);
            dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                   GTK_MESSAGE_ERROR,
                                                                   GTK_BUTTONS_OK,
                                                                   _("warn_error")));
            gtk_message_dialog_format_secondary_text(dialog_box, _("error_disconnect_error"));
            g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
            gtk_window_present(GTK_WINDOW(dialog_box));
        }

        // Unlock UI and free context
        xr25_free_diagnostic_context(context->diagnostic_context);
        application_unlock_ui(context);
        return;
    }
    g_error("Unreachable in theory! Diagnostic button.");
}

gboolean application_refresh_timeout(gpointer user_data)
{
    // Local variables
    ApplicationContext *context;
    GtkMessageDialog *dialog_box;
    XR25DiagnosticContext *context_copy;
    GXR25Parameter *param;
    char buffer[STRING_BUFFER_SIZE];
    int count, index;
    unsigned long time;

    // Get context
    context = (ApplicationContext *)user_data;

    // Check application status
    if (context->diagnostic_state == DiagnosticStateYes)
    {
        // Check diagnostic status
        if (context->diagnostic_context->is_connected == XR25ConnectionStatusConnected)
        {
            // Timer for checking performence
            time = xr25_get_timestamp_msec();

            // Update only when it is needed
            if (context->diagnostic_context->last_tick > context->last_update)
            {
                // Update local variable
                context->last_update = context->diagnostic_context->last_tick;

                // Copy context
                context_copy = malloc(sizeof(XR25DiagnosticContext));
                pthread_mutex_lock(context->diagnostic_context->diagnostic_mutex);
                memcpy(context_copy, context->diagnostic_context, sizeof(XR25DiagnosticContext));
                context_copy->priv_data = malloc(context_copy->card->priv_data_size);
                memcpy(context_copy->priv_data, context->diagnostic_context->priv_data, context_copy->card->priv_data_size);
                pthread_mutex_unlock(context->diagnostic_context->diagnostic_mutex);

                // Update list
                count = g_list_model_get_n_items(G_LIST_MODEL(context->liststore_parameter));
                for (index = 0; index < count; index++)
                {
                    param = g_list_model_get_item(G_LIST_MODEL(context->liststore_parameter), index);
                    context_copy->card->parameter_list[index]->get_value(context_copy, buffer);
                    gxr25_parameter_set_value(param, buffer);
                }
                g_list_model_items_changed(G_LIST_MODEL(context->liststore_parameter), 0, 0, 0); // list refresh

                // Free resources
                free(context_copy->priv_data);
                free(context_copy);
            }

            // Warn about issues
            time = xr25_get_timestamp_msec() - time;
            if (time > TICK_REFRESH_WARN_MSEC && time <= TICK_REFRESH_CRITICAL_MSEC)
                g_critical("Timeout took too long - warning - %ldms", time);
            if (time > TICK_REFRESH_CRITICAL_MSEC)
                g_critical("Timeout took too long - critical - %ldms", time);

            return G_SOURCE_CONTINUE;
        }
        else
        {
            // App is in diagnostic state but probe isn't
            g_critical("We are not in connected state - timeout exiting..");
            g_critical("Thread exit reason: %d", context->diagnostic_context->thread_exit_value);
            dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                   GTK_MESSAGE_ERROR,
                                                                   GTK_BUTTONS_OK,
                                                                   _("warn_error")));
            gtk_message_dialog_format_secondary_text(dialog_box, _("error_disconnected_error"));
            g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
            gtk_window_present(GTK_WINDOW(dialog_box));
            application_startstop_diag_signal(NULL, user_data);
            return G_SOURCE_REMOVE;
        }
    }
    else
    {
        g_critical("We are not in diagnostic state - timeout exiting..");
        return G_SOURCE_REMOVE;
    }
}

void application_lock_ui(ApplicationContext *context)
{
    context->diagnostic_state = DiagnosticStateYes;
    gtk_widget_set_sensitive(GTK_WIDGET(context->button_diagnostic_card_select), false);
    gtk_widget_set_sensitive(GTK_WIDGET(context->button_interface_select), false);
    gtk_button_set_label(context->button_startstop_diag, _("button_stop_diag"));
}

void application_unlock_ui(ApplicationContext *context)
{
    gtk_widget_set_sensitive(GTK_WIDGET(context->button_interface_select), true);
    gtk_widget_set_sensitive(GTK_WIDGET(context->button_diagnostic_card_select), true);
    gtk_button_set_label(context->button_startstop_diag, _("button_start_diag"));
    context->diagnostic_state = DiagnosticStateNo;
}