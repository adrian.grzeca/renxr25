#ifndef __ABOUTWINDOW__
#define __ABOUTWINDOW__

#include <gtk/gtk.h>

#include "application.h"

void aboutwindow_show(ApplicationContext *context);

#endif //__ABOUTWINDOW__