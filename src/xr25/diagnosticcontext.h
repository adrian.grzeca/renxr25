#ifndef __XR25DIAGNOSTICCONTEXT__
#define __XR25DIAGNOSTICCONTEXT__

#include <ftdi.h>
#include <pthread.h>

// Circular struct dependency
typedef struct XR25DiagnosticCard XR25DiagnosticCard;

// Defines connection state
typedef enum XR25ConnectionStatus
{
    XR25ConnectionStatusConnected,
    XR25ConnectionStatusConnectedExiting,
    XR25ConnectionStatusConnectedThreadExited,
    XR25ConnectionStatusDisconnected
} XR25ConnectionStatus;

// Context for diagnostics
typedef struct XR25DiagnosticContext
{
    XR25ConnectionStatus is_connected;
    XR25DiagnosticCard *card;
    pthread_t diagnostic_thread;
    pthread_mutex_t *diagnostic_mutex;
    int thread_exit_value;
    struct ftdi_context *ftdi;
    int interface_bus;
    int interface_port;
    unsigned long last_tick; // msec
    unsigned long last_tick_wall; // msec
    void *priv_data;
} XR25DiagnosticContext;

const XR25DiagnosticContext *xr25_malloc_diagnostic_context(const XR25DiagnosticCard *card, struct ftdi_context *ftdi, int interface_bus, int interface_port);
void xr25_free_diagnostic_context(const XR25DiagnosticContext *context);
int xr25_connect(XR25DiagnosticContext *context);
int xr25_disconnect(XR25DiagnosticContext *context);
int xr25_start_worker(const XR25DiagnosticContext *context);
int xr25_send_request(const XR25DiagnosticContext *context, const char *data);
void *xr25_thread_worker(void *data);
unsigned long xr25_get_timestamp_msec();
unsigned long xr25_get_timestamp_usec();
unsigned long xr25_get_timestamp_msec_wall();
unsigned long xr25_get_timestamp_usec_wall();

#endif //__XR25DIAGNOSTICCONTEXT__