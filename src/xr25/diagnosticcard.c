#include <glib/gi18n.h>
#include <string.h>

#include "diagnosticcard.h"

// Header "file" for diagnostic card!
// If you declare diagnostic card add it here!!
// extern const DiagnosticCard diagnostic__syp_renault_safrane_ph1;
// extern const DiagnosticCard diagnostic_2syp_renault_safrane_ph1;
extern const XR25DiagnosticCard diagnostic_2syp_renault_safrane_ph2;
extern const XR25DiagnosticCard diagnostic_2ult_renault_safrane_ph2;

// Diagnostic card static list
// IT MUST TERMINATE WITH NULL!!
static const XR25DiagnosticCard *const diagnostic_cards[] = {
    //&diagnostic__syp_renault_safrane_ph1,
    //&diagnostic_2syp_renault_safrane_ph1,
    &diagnostic_2syp_renault_safrane_ph2,
    &diagnostic_2ult_renault_safrane_ph2,
    NULL,
};

// Macro for count of elements in diagnostic_cards arrary
#define DIAGNOSTIC_CARDS_COUNT (sizeof(diagnostic_cards) / sizeof(diagnostic_cards[0]))

// Returns computer type string.
const char *xr25_get_computer_type_string(XR25ComputerType computer)
{
    switch (computer)
    {
    case XR25ComputerTypeDashboard:
        return _("label_computer_dashboard");
    case XR25ComputerTypeCruiseControl:
        return _("label_computer_cruise_control");
    case XR25ComputerTypePetrolInjection:
        return _("label_computer_petrol_injection");
    case XR25ComputerTypeDiselInjection:
        return _("label_computer_diesel_injection");
    default:
        return _("label_computer_invalid");
    }
}

// Returns cable type string.
const char *xr25_get_cable_type_string(XR25CableType cable)
{
    switch (cable)
    {
    case XR25CableTypeCableS1ODB1:
        return _("label_cable_type_s1_odb1");
    case XR25CableTypeCableS2ODB1:
        return _("label_cable_type_s2_odb1");
    case XR25CableTypeCableS3ODB1:
        return _("label_cable_type_s3_odb1");
    case XR25CableTypeCableS4ODB1:
        return _("label_cable_type_s4_odb1");
    case XR25CableTypeCableS5ODB1:
        return _("label_cable_type_s5_odb1");
    case XR25CableTypeCableS6ODB1:
        return _("label_cable_type_s6_odb1");
    case XR25CableTypeCableS7ODB1:
        return _("label_cable_type_s7_odb1");
    case XR25CableTypeCableS67ODB1:
        return _("label_cable_type_s67_odb1");
    case XR25CableTypeCableS8ODB1:
        return _("label_cable_type_s8_odb1");
    case XR25CableTypeCableS8ODB2:
        return _("label_cable_type_s8_odb2");
    case XR25CableTypeCableS8ODB12:
        return _("label_cable_type_s8_odb12");
    default:
        return _("label_cable_type_invalid");
    }
}

// Return car manufaturer string.
const char *xr25_get_car_manufacturer_string(XR25CarManufacturer manufacturer)
{
    switch (manufacturer)
    {
    case XR25CarManufacturerRenault:
        return _("label_manufacturer_renault");
    default:
        return _("label_manufacturer_invalid");
    }
}
// Returns car model string.
const char *xr25_get_car_model_string(XR25CarManufacturer manufacturer, XR25CarModel model)
{
    switch (manufacturer)
    {
    case XR25CarManufacturerRenault:
        switch (model)
        {
        case XR25CarModelRenault1X54SafranePh1:
            return _("label_model_renault1x54safraneph1");
        case XR25CarModelRenault1X54SafranePh2:
            return _("label_model_renault1x54safraneph2");
        case XR25CarModelRenault1X56Laguna1Ph1:
            return _("label_model_renault1x56lagunaph1");
        case XR25CarModelRenault1X56Laguna1Ph2:
            return _("label_model_renault1x56lagunaph2");
        default:
            return _("label_model_invalid");
        }
    default:
        return _("label_model_invalid");
    }
}

// Returns count of registred diagnostics cards
// It includes NULL card!
int xr25_get_diagnotic_card_count()
{
    return (int)DIAGNOSTIC_CARDS_COUNT;
}

// Returns copy of diagnostic_cards arrary
// Caller is responsible for disposing it
// It uses stdlib alloc calls
const XR25DiagnosticCard **xr25_get_diagnostic_card_list()
{
    const XR25DiagnosticCard **ret;
    ret = malloc(sizeof(diagnostic_cards));
    memcpy(ret, diagnostic_cards, sizeof(diagnostic_cards));
    return ret;
}

// Returns diagnostic card by code.
// There could be multiple cards with same code!
// In such case it will return first one - it should compatible.
const XR25DiagnosticCard *xr25_get_diagnostic_card_by_code(const char *code)
{
    // Declare variables
    int i = 0;

    // Search for card
    for (i = 0; diagnostic_cards[i] != NULL; i++)
    {
        if (strcmp(diagnostic_cards[i]->code, code) == 0)
            return diagnostic_cards[i];
    }

    // Card not found
    return NULL;
}

// Returns diagnostic card by unique code.
const XR25DiagnosticCard *xr25_get_diagnostic_card_by_unique_code(const char *unique_code)
{
    // Declare variables
    int i = 0;

    // Search for card
    for (i = 0; diagnostic_cards[i] != NULL; i++)
    {
        if (strcmp(diagnostic_cards[i]->unique_code, unique_code) == 0)
            return diagnostic_cards[i];
    }

    // Card not found
    return NULL;
}

// Returns diagnostic card by index in global list.
const XR25DiagnosticCard *xr25_get_diagnostic_card_by_index(int index)
{
    if (index < 0 || index >= DIAGNOSTIC_CARDS_COUNT)
        return NULL;
    return diagnostic_cards[index];
}