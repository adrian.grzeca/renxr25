#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <ftdi.h>
#include <time.h>
#include <glib/gi18n.h> // I need get text here..

#include "common.h"
#include "../xr25.h"

// Common packets
const unsigned char xr25_packet_status_packet[] = {0x03, 0x31, 0x50, 0x84};
#define XR25_STATUS_PACKET_LENGHT (sizeof(xr25_packet_status_packet) / sizeof(xr25_packet_status_packet[0]))
const int xr25_packet_status_packet_size = XR25_STATUS_PACKET_LENGHT;

// Common params
const XR25DiagnosticCardParameter xr25_param_last_update = {
    .name = "Last update",
    .label = X_("label_last_update"),
    .unit = "",
    .get_value = &xr25_param_get_last_update,
};

int xr25_set_iso_state(const XR25DiagnosticContext *context, int state)
{
    // Variables
    int ret;

    // Set ftdi
    ret = ftdi_set_line_property2(context->ftdi, BITS_8, STOP_BIT_1, NONE, state ? BREAK_OFF : BREAK_ON); // OFF = High
    if (ret < 0)
    {
        // ftdi_set_line_property2 fail
        return -1;
    }

    // Success
    return 0;
}

int xr25_connect_to_address(const XR25DiagnosticContext *context)
{
    // Local variables
    int i, byte, address, safety_counter, ret, connected, calculator_id;
    unsigned char buffer;

    // Assignment
    address = context->card->address;
    connected = XR25_CONNECTED_NO;
    calculator_id = 0;

    // Idle bus
    if (xr25_set_iso_state(context, XR25_ISO_HIGH) != 0)
    {
        // Starting fail - stage 1
        return -1;
    }

    // Bus idle - 100ms (long..)
    usleep(100000);

    // Pull low for 200ms
    if (xr25_set_iso_state(context, XR25_ISO_LOW) != 0)
    {
        // Starting fail - stage 2
        return -2;
    }

    // Bus idle - 200ms (long..)
    usleep(200000);

    // send device address
    // 8 bit long
    for (i = 0; i < 8; i++)
    {
        byte = address & 0x1;                                                         // get last bit
        address = address >> 1;                                                       // move addr to right
        if (xr25_set_iso_state(context, (!byte) ? XR25_ISO_LOW : XR25_ISO_HIGH) != 0) // sending
        {
            // Starting fail - stage 3
            return -3;
        }
        // Bus idle - 200ms
        usleep(200000);
    }

    // pull high
    if (xr25_set_iso_state(context, XR25_ISO_HIGH) != 0)
    {
        // Starting fail - stage 4
        return -4;
    }

    // Bus idle - 750ms - must be very long
    usleep(750000);

    // Read computer ID
    for (safety_counter = 0; safety_counter < XR25_SAFETY_COUNTER_LOW; safety_counter++)
    {
        ret = ftdi_read_data(context->ftdi, &buffer, 1); // read one char!
        if (ret < 0)                                     // ftdi/usb error
        {
            // Data error - ftdi
            return -5;
        }
        else if (ret == 0 && safety_counter == 0) // if first read fails there is kline error
        {
            // Data error - kline
            return -6;
        }
        else if (ret == 0 && safety_counter != 0) // no more data!
        {
            break;
        }
        else // we read some data
        {
            if (connected == XR25_CONNECTED_YES) // we are connected and we searching for computer id
            {
                calculator_id = (calculator_id << 8) | buffer;
            }
            if (buffer == 0x55 && connected == XR25_CONNECTED_NO) // we are not connected and we found ack
            {
                connected = XR25_CONNECTED_YES;
            }
        }
    }

    // Check received calculator ID
    if (calculator_id != context->card->calculator_id)
    {
        // Invalid calculator id
        return -7;
    }

    return 0;
}

int xr25_param_get_empty(XR25DiagnosticContext *context, char *output_string)
{
    strcpy(output_string, "--");
    return 0;
}

int xr25_param_get_last_update(XR25DiagnosticContext *context, char *output_string)
{
    char buffer[32];
    time_t tm;
    int msec;

    tm = context->last_tick_wall / 1000;
    msec = context->last_tick_wall % 1000;
    strftime(buffer, 20, "%Y-%m-%d %H:%M:%S", localtime(&tm));
    g_snprintf(output_string, XR25_PARAMETER_STRING_MAX_LENGTH, "%s:%03d", buffer, msec);

    return 0;
}

int xr25_param_helper_yes_no(char *output_string, int value)
{
    if (value == 0)
        g_utf8_strncpy(output_string, _("diagnostic_no"), XR25_PARAMETER_STRING_MAX_LENGTH);
    else if (value == 1)
        g_utf8_strncpy(output_string, _("diagnostic_yes"), XR25_PARAMETER_STRING_MAX_LENGTH);
    else
        g_utf8_strncpy(output_string, _("diagnostic_invalid"), XR25_PARAMETER_STRING_MAX_LENGTH);
    ;

    return 0;
}