#include <string.h>
#include <glib/gi18n.h> // I need get text here..
#include <ftdi.h>
#include <stdint.h>
#include <unistd.h>

#include "../xr25.h"
#include "common.h"

#define DIAGNOSTIC_2ULT_RENAULT_BUFFER_SIZE 64

typedef struct diagnostic_2ult_renault_priv_data
{
    unsigned char buffer[DIAGNOSTIC_2ULT_RENAULT_BUFFER_SIZE];
    unsigned char counter;
    unsigned char part_number_1;
    unsigned char part_number_2;
    unsigned char part_number_3;
    unsigned char part_number_4;
    unsigned char part_number_5;
    unsigned char version;
    unsigned char etat; // WTF is this?!
    unsigned char day;
    unsigned char month;
    unsigned char year;
    unsigned char eeprom;
} diagnostic_2ult_renault_priv_data;

void diagnostic_2ult_renault_initialize(const XR25DiagnosticContext *context)
{
    // Get context
    diagnostic_2ult_renault_priv_data *priv_data;
    priv_data = (diagnostic_2ult_renault_priv_data *)context->priv_data;

    // Initialize private data
    priv_data->counter = 0;
    priv_data->part_number_1 = -1;
    priv_data->part_number_2 = -1;
    priv_data->part_number_3 = -1;
    priv_data->part_number_4 = -1;
    priv_data->part_number_5 = -1;
    priv_data->version = -1;
}

void diagnostic_2ult_renault_deinitialize(const XR25DiagnosticContext *context)
{
}

int diagnostic_2ult_renault_connect(const XR25DiagnosticContext *context)
{
    return xr25_connect_to_address(context);
}

int diagnostic_2ult_renault_disconnect(const XR25DiagnosticContext *context)
{
    return 0;
}

int diagnostic_2ult_renault_tick(const XR25DiagnosticContext *context)
{
    // Local variables
    diagnostic_2ult_renault_priv_data *priv_data;
    int ret;

    // Assign private data
    priv_data = (diagnostic_2ult_renault_priv_data *)context->priv_data;

    // Send status request
    ret = ftdi_write_data(context->ftdi, xr25_packet_status_packet, xr25_packet_status_packet_size);
    if (ret != xr25_packet_status_packet_size)
    {
        // Failed to send request
        return -1;
    }
    usleep(5000);                 // small sleep (5ms)
    ftdi_tciflush(context->ftdi); // flush echos
    usleep(20000);                // wait for response (20ms)

    // Read status request
    ret = ftdi_read_data(context->ftdi, priv_data->buffer, DIAGNOSTIC_2ULT_RENAULT_BUFFER_SIZE);
    if (ret < 0)
    {
        // Read fail
        return -2;
    }
    else if (ret > 0)
    {
        if (priv_data->buffer[0] + 1 == ret)
        {
            // Read ok
            priv_data->counter = 0;

            // Read part number..
            priv_data->part_number_1 = priv_data->buffer[3];
            priv_data->part_number_2 = priv_data->buffer[4];
            priv_data->part_number_3 = priv_data->buffer[5];
            priv_data->part_number_4 = priv_data->buffer[6];
            priv_data->part_number_5 = priv_data->buffer[7];

            // Read version
            priv_data->version = priv_data->buffer[8];
        }
    }
    else
    {
        // No data!
        if ((priv_data->counter++) > 20)
            return -3;
    }

    // Return
    return 0;
}

int diagnostic_2ult_renault_get_part_number(XR25DiagnosticContext *context, char *output_string)
{
    diagnostic_2ult_renault_priv_data *priv_data;
    priv_data = (diagnostic_2ult_renault_priv_data *)context->priv_data;
    g_snprintf(output_string, 32, "%02X %02X %02X %02X %02X",
               priv_data->part_number_1,
               priv_data->part_number_2,
               priv_data->part_number_3,
               priv_data->part_number_4,
               priv_data->part_number_5);
    return 0;
}

int diagnostic_2ult_renault_get_version(XR25DiagnosticContext *context, char *output_string)
{
    diagnostic_2ult_renault_priv_data *priv_data;
    priv_data = (diagnostic_2ult_renault_priv_data *)context->priv_data;
    g_snprintf(output_string, 8, "%02X", priv_data->version);
    return 0;
}

const XR25DiagnosticCardParameter diagnostic_2ult_renault_param_part_number = {
    .name = "Part number",
    .label = X_("label_part_number"),
    .unit = "",
    .get_value = &diagnostic_2ult_renault_get_part_number,
};

const XR25DiagnosticCardParameter diagnostic_2ult_renault_param_version = {
    .name = "Version",
    .label = X_("label_version"),
    .unit = "",
    .get_value = &diagnostic_2ult_renault_get_version,
};

const XR25DiagnosticCardParameter diagnostic_2ult_renault_param_day = {
    .name = "Day",
    .label = X_("label_part_number"),
    .unit = "",
    .get_value = &diagnostic_2ult_renault_get_part_number,
};

const XR25DiagnosticCard diagnostic_2ult_renault_safrane_ph2 = {
    .code = "2.ult",
    .unique_code = "2.ULT-X54-PH2",
    .type = XR25ComputerTypeCruiseControl,
    .car_manufacturer = XR25CarManufacturerRenault,
    .car_model = XR25CarModelRenault1X54SafranePh2,
    .cable_type = XR25CableTypeCableS8ODB2,
    .baud_rate = 10400,
    .address = 0xAB,
    .calculator_id = 0x8083,
    .priv_data_size = sizeof(diagnostic_2ult_renault_priv_data),
    .initialize = diagnostic_2ult_renault_initialize,
    .deinitialize = diagnostic_2ult_renault_deinitialize,
    .connect = diagnostic_2ult_renault_connect,
    .disconnect = diagnostic_2ult_renault_disconnect,
    .tick_data = diagnostic_2ult_renault_tick,
    .parameter_list = {
        &xr25_param_last_update,
        &diagnostic_2ult_renault_param_part_number,
        &diagnostic_2ult_renault_param_version,
        NULL,
    },
};