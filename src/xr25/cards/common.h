#ifndef __XR25CARDSCOMMON__
#define __XR25CARDSCOMMON__

#include "../diagnosticcard.h"

// Common packets
extern const unsigned char xr25_packet_status_packet[];
extern const int xr25_packet_status_packet_size;

// Common params
extern const XR25DiagnosticCardParameter xr25_param_last_update;

// Macro for labels
#define X_(String) (String)

// Macro for states
#define XR25_SAFETY_COUNTER_LOW 64
#define XR25_ISO_HIGH 1
#define XR25_ISO_LOW 0
#define XR25_CONNECTED_YES 1
#define XR25_CONNECTED_NO 0

// ISO manipulation functions
int xr25_set_iso_state(const XR25DiagnosticContext *context, int state);
int xr25_connect_to_address(const XR25DiagnosticContext *context);

// Parameters generic getters
int xr25_param_get_empty(XR25DiagnosticContext *context, char *output_string);
int xr25_param_get_last_update(XR25DiagnosticContext *context, char *output_string);

// Helpers for getters
int xr25_param_helper_yes_no(char *output_string, int value);

#endif //__XR25CARDSCOMMON__