#include <string.h>
#include <glib/gi18n.h> // I need get text here..
#include <ftdi.h>
#include <unistd.h>

#include "../xr25.h"
#include "common.h"

#define DIAGNOSTIC_2SYP_RENAULT_BUFFER_SIZE 64

typedef struct diagnostic_2syp_renault_priv_data
{
    // Data used by loop
    unsigned char buffer[DIAGNOSTIC_2SYP_RENAULT_BUFFER_SIZE];
    unsigned char counter;

    // Data from computer
    unsigned char signal_abs;
    unsigned char signal_automatic_gearbox;
    unsigned char signal_amv_vpas; // VPAS = Variable Power Assisted Steering
    unsigned char signal_automatic_air_conditioning;
    unsigned char signal_fuel_injection;
    unsigned char signal_plus_apc;
    unsigned char signal_plus_lights;
    unsigned char signal_hood_open;
    unsigned char signal_left_front_door_open;
    unsigned char signal_right_front_door_open;
    unsigned char signal_left_rear_door_open;
    unsigned char signal_right_rear_door_open;
    unsigned char signal_trunk_open;
    unsigned char signal_driver_seat_belt;
    unsigned char signal_handbrake;
    unsigned char signal_low_fuel_level;
    unsigned char signal_low_windshield_washer_fluid;
    unsigned char signal_low_brake_fluid;
    unsigned char signal_low_oil_level;
    unsigned char signal_brake_pads;
    unsigned char signal_low_oil_pressure;          // it is signal mostly but also fault..
    unsigned char signal_engine_speed_above_500rpm; // running engine signal
    unsigned char signal_speed_above_15kmh;         // driving signal
    unsigned char signal_engine_overheat;
    unsigned char signal_quadra_diff_lock;
    unsigned char signal_button_discretion;
    unsigned char signal_button_repeat;
    unsigned char signal_fault_alternator;
    unsigned char signal_fault_side_lights;
    unsigned char signal_fault_left_stop_light;
    unsigned char signal_fault_middle_stop_light; // not connected LOL
    unsigned char signal_fault_right_stop_light;
    unsigned char signal_fault_tire_pressure;
    unsigned char flag_gen2_immo;
    unsigned char config_language;
    unsigned char config_gen2_immo;
    unsigned char version;
} diagnostic_2syp_renault_priv_data;

void diagnostic_2syp_renault_initialize(const XR25DiagnosticContext *context)
{
    // Get context
    diagnostic_2syp_renault_priv_data *priv_data;
    priv_data = (diagnostic_2syp_renault_priv_data *)context->priv_data;

    // Initialize private data
    memset(priv_data, 0, sizeof(priv_data));
}

void diagnostic_2syp_renault_deinitialize(const XR25DiagnosticContext *context)
{
}

int diagnostic_2syp_renault_connect(const XR25DiagnosticContext *context)
{
    return xr25_connect_to_address(context);
}

int diagnostic_2syp_renault_disconnect(const XR25DiagnosticContext *context)
{
    return 0;
}

int diagnostic_2syp_renault_tick(const XR25DiagnosticContext *context)
{
    // Local variables
    diagnostic_2syp_renault_priv_data *priv_data;
    int ret;

    // Assign private data
    priv_data = (diagnostic_2syp_renault_priv_data *)context->priv_data;

    // Send status request
    ret = ftdi_write_data(context->ftdi, xr25_packet_status_packet, xr25_packet_status_packet_size);
    if (ret != xr25_packet_status_packet_size)
    {
        // Failed to send request
        return -1;
    }
    usleep(25000);                // small sleep (25ms)
    ftdi_tciflush(context->ftdi); // flush echos
    usleep(60000);                // wait for response (60ms)

    // Read status request
    ret = ftdi_read_data(context->ftdi, priv_data->buffer, DIAGNOSTIC_2SYP_RENAULT_BUFFER_SIZE);
    if (ret < 0)
    {
        // Read fail
        return -2;
    }
    else if (ret > 0)
    {
        if (priv_data->buffer[0] + 1 == ret)
        {
            // Read ok - reset counter
            priv_data->counter = 0;

            // Reading data

            // Byte [3] - All used
            priv_data->version = (priv_data->buffer[3] & 0xF0) >> 4;
            priv_data->config_language = priv_data->buffer[3] & 0x0F;

            // Byte [4] - Unused: 3
            priv_data->signal_engine_overheat = (priv_data->buffer[4] & 0x80) >> 7;
            priv_data->signal_automatic_air_conditioning = (priv_data->buffer[4] & 0x40) >> 6;
            priv_data->signal_brake_pads = (priv_data->buffer[4] & 0x20) >> 5;
            priv_data->signal_handbrake = (priv_data->buffer[4] & 0x10) >> 4;
            // unused bit 3
            priv_data->signal_low_oil_pressure = (priv_data->buffer[4] & 0x04) >> 2;
            priv_data->signal_low_brake_fluid = (priv_data->buffer[4] & 0x02) >> 1;
            priv_data->signal_automatic_gearbox = priv_data->buffer[4] & 0x01;

            // Byte [5] - All used
            priv_data->signal_left_front_door_open = (priv_data->buffer[5] & 0x80) >> 7;
            priv_data->signal_right_front_door_open = (priv_data->buffer[5] & 0x40) >> 6;
            priv_data->signal_left_rear_door_open = (priv_data->buffer[5] & 0x20) >> 5;
            priv_data->signal_right_rear_door_open = (priv_data->buffer[5] & 0x10) >> 4;
            priv_data->signal_trunk_open = (priv_data->buffer[5] & 0x08) >> 3;
            priv_data->signal_hood_open = (priv_data->buffer[5] & 0x04) >> 2;
            priv_data->signal_button_repeat = (priv_data->buffer[5] & 0x02) >> 1;
            priv_data->signal_button_discretion = priv_data->buffer[5] & 0x01;

            // Byte [6] - All used
            priv_data->signal_plus_lights = (priv_data->buffer[6] & 0x80) >> 7;
            priv_data->signal_fault_side_lights = (priv_data->buffer[6] & 0x40) >> 6;
            priv_data->signal_fault_left_stop_light = (priv_data->buffer[6] & 0x20) >> 5;
            priv_data->signal_fault_right_stop_light = (priv_data->buffer[6] & 0x10) >> 4;
            priv_data->signal_fault_middle_stop_light = (priv_data->buffer[6] & 0x08) >> 3;
            priv_data->signal_low_fuel_level = (priv_data->buffer[6] & 0x04) >> 2;
            priv_data->signal_fuel_injection = (priv_data->buffer[6] & 0x02) >> 1;
            priv_data->signal_fault_alternator = priv_data->buffer[6] & 0x01;

            // Byte [7] - All used
            priv_data->signal_abs = (priv_data->buffer[7] & 0x80) >> 7;
            priv_data->signal_fault_tire_pressure = (priv_data->buffer[7] & 0x40) >> 6;
            priv_data->signal_quadra_diff_lock = (priv_data->buffer[7] & 0x20) >> 5;
            priv_data->signal_plus_apc = (priv_data->buffer[7] & 0x10) >> 4;
            priv_data->config_gen2_immo =(priv_data->buffer[7] & 0x08) >> 3;
            priv_data->signal_amv_vpas = (priv_data->buffer[7] & 0x04) >> 2;
            priv_data->signal_driver_seat_belt = (priv_data->buffer[7] & 0x02) >> 1;
            priv_data->signal_low_windshield_washer_fluid = priv_data->buffer[7] & 0x01;

            // Byte [8] - Unused: 2, 5, 6, 7
            // unused bit 7
            // unused bit 6
            // unused bit 5
            priv_data->signal_speed_above_15kmh = (priv_data->buffer[8] & 0x10) >> 4;
            priv_data->signal_engine_speed_above_500rpm = (priv_data->buffer[8] & 0x08) >> 3;
            // unused bit 2
            priv_data->flag_gen2_immo = (priv_data->buffer[8] & 0x02) >> 1;
            priv_data->signal_low_oil_level = priv_data->buffer[8] & 0x01;
        }
    }
    else
    {
        // Disconnect after 20 no data events
        if ((priv_data->counter++) > 20)
            return -3;
    }

    // Return
    return 0;
}

int diagnostic_2syp_renault_get_key_rep(XR25DiagnosticContext *context, char *output_string)
{
    diagnostic_2syp_renault_priv_data *priv_data;
    priv_data = (diagnostic_2syp_renault_priv_data *)context->priv_data;
    return xr25_param_helper_yes_no(output_string, priv_data->signal_button_repeat);
}

int diagnostic_2syp_renault_get_lang(XR25DiagnosticContext *context, char *output_string)
{
    diagnostic_2syp_renault_priv_data *priv_data;
    priv_data = (diagnostic_2syp_renault_priv_data *)context->priv_data;

    switch (priv_data->config_language)
    {
    case 0x01:
        strcpy(output_string, _("diagnostic_2syp_renault_lang_french"));
        break;
    case 0x03:
        strcpy(output_string, _("diagnostic_2syp_renault_lang_english"));
        break;
    case 0x05:
        strcpy(output_string, _("diagnostic_2syp_renault_lang_german"));
        break;
    case 0x07:
        strcpy(output_string, _("diagnostic_2syp_renault_lang_pasta"));
        break;
    case 0x09:
        strcpy(output_string, _("diagnostic_2syp_renault_lang_mexican"));
        break;
    case 0x0B:
        strcpy(output_string, _("diagnostic_2syp_renault_lang_funny_german"));
        break;
    }

    return 0;
}

const XR25DiagnosticCardParameter diagnostic_2syp_renault_param_repkey = {
    .name = "Rep. Key.",
    .label = X_("label_param_repkey"),
    .unit = "",
    .get_value = &diagnostic_2syp_renault_get_key_rep,
};
const XR25DiagnosticCardParameter diagnostic_2syp_renault_param_lang = {
    .name = "Language",
    .label = X_("label_param_lang"),
    .unit = "",
    .get_value = &diagnostic_2syp_renault_get_lang,
};

const XR25DiagnosticCard diagnostic_2syp_renault_safrane_ph2 = {
    .code = "2.SYP",
    .unique_code = "2.SYP-PH2",
    .type = XR25ComputerTypeDashboard,
    .car_manufacturer = XR25CarManufacturerRenault,
    .car_model = XR25CarModelRenault1X54SafranePh2,
    .cable_type = XR25CableTypeCableS8ODB2,
    .baud_rate = 10400,
    .address = 0x52,
    .calculator_id = 0x7086,
    .priv_data_size = sizeof(diagnostic_2syp_renault_priv_data),
    .initialize = diagnostic_2syp_renault_initialize,
    .deinitialize = diagnostic_2syp_renault_deinitialize,
    .connect = diagnostic_2syp_renault_connect,
    .disconnect = diagnostic_2syp_renault_disconnect,
    .tick_data = diagnostic_2syp_renault_tick,
    .parameter_list = {
        &xr25_param_last_update,
        &diagnostic_2syp_renault_param_repkey,
        &diagnostic_2syp_renault_param_lang,
        NULL,
    },
};

/*

// Left overs from early development but it will be used in future

const DiagnosticCard diagnostic__syp_renault_safrane_ph1 = {
    .code = "_.SYP",
    .unique_code = "_.SYP",
    .type = XR25ComputerTypeDashboard,
    .car_manufacturer = XR25CarManufacturerRenault,
    .car_model = XR25CarModelRenault1X54SafranePh1,
    .cable_type = XR25CableTypeCableS8ODB1,
    .baud_rate = 2400,
    .address = 0x52,
    .calculator_id = 0x7F80,
    .parameter_list = {NULL},
};

const DiagnosticCard diagnostic_2syp_renault_safrane_ph1 = {
    .code = "2.SYP",
    .unique_code = "2.SYP-PH1",1080000
    .type = XR25ComputerTypeDashboard,
    .car_manufacturer = XR25CarManufacturerRenault,
    .car_model = XR25CarModelRenault1X54SafranePh1, // Could be found in Ph1 Safrane!
    .cable_type = XR25CableTypeCableS8ODB1,
    .baud_rate = 10400,
    .address = 0x52,
    .calculator_id = 0x7086,
    .parameter_list = {NULL},
};
*/