#include <string.h>
#include <ftdi.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>

#include "config.h"
#include "diagnosticcard.h"
#include "diagnosticcontext.h"

// Allocate diagnostic context - Free is handled by dedicated function
const XR25DiagnosticContext *xr25_malloc_diagnostic_context(const XR25DiagnosticCard *card, struct ftdi_context *ftdi, int interface_bus, int interface_port)
{
    XR25DiagnosticContext *diagnostic_context;
    diagnostic_context = malloc(sizeof(XR25DiagnosticContext));
    diagnostic_context->is_connected = XR25ConnectionStatusDisconnected;
    diagnostic_context->card = (XR25DiagnosticCard *)card;
    diagnostic_context->diagnostic_mutex = malloc(sizeof(pthread_mutex_t));
    diagnostic_context->thread_exit_value = 0;
    pthread_mutex_init(diagnostic_context->diagnostic_mutex, NULL);
    diagnostic_context->ftdi = ftdi;
    diagnostic_context->interface_bus = interface_bus;
    diagnostic_context->interface_port = interface_port;
    diagnostic_context->last_tick = 0;
    diagnostic_context->priv_data = malloc(card->priv_data_size);
    card->initialize(diagnostic_context);
    return diagnostic_context;
}

// Deallocate diagnostic context
void xr25_free_diagnostic_context(const XR25DiagnosticContext *context)
{
    // Sanity check
    if (context == NULL)
        return;

    // Check if connection has been closed
    if (context->is_connected != XR25ConnectionStatusDisconnected)
        xr25_disconnect((XR25DiagnosticContext *)context);

    // Deinitialize
    context->card->deinitialize(context); // Do not close FTDI - give chance to close diagnostics
    ftdi_usb_close(context->ftdi);
    pthread_mutex_destroy(context->diagnostic_mutex);
    free(context->diagnostic_mutex);
    free(context->priv_data);
    free((void *)context);
}

// Connect to calculator selected in context
int xr25_connect(XR25DiagnosticContext *context)
{
    // Local variables
    int ret;

    // Sanity check
    if (context == NULL)
        return -1;

    // Open FTDI
    ret = ftdi_usb_open_bus_addr(context->ftdi, context->interface_bus, context->interface_port);
    if (ret != 0)
    {
        // Failed to open FTDI
        return ret - 100;
    }

    // Set baudrate
    ftdi_set_baudrate(context->ftdi, context->card->baud_rate);
    if (ret != 0)
    {
        // Failed to set baudrate
        ftdi_usb_close(context->ftdi);
        return ret - 200;
    }

    // Flush buffer
    ftdi_tciflush(context->ftdi);

    // Try to connect - do not care about fail
    ret = context->card->connect(context);
    if (ret == 0)
    {
        context->is_connected = XR25ConnectionStatusConnected;
        return 0;
    }

    // Failed to connect
    ftdi_usb_close(context->ftdi);
    return ret;
}

// Disconnect from calculator
int xr25_disconnect(XR25DiagnosticContext *context)
{
    // Local variables
    int ret;
    int count;

    // Sanity check
    if (context == NULL)
        return -1;

    // Pre disconnect - it should end thread
    context->is_connected = XR25ConnectionStatusConnectedExiting;

    // Check if thread has ended
    for (count = 0; count < XR25_THREAD_WAIT_COUNT; count++)
    {
        usleep(XR25_THREAD_WAIT_TICK_TIME_USEC);
        if (context->is_connected == XR25ConnectionStatusConnectedThreadExited)
            break;
    }
    if (count == XR25_THREAD_WAIT_COUNT)
        pthread_cancel(context->diagnostic_thread); // Try to kill thread

    // Disconnect - No matter what happend we are in disconnected state
    ret = context->card->disconnect(context);
    context->is_connected = XR25ConnectionStatusDisconnected;

    // Return return value of disconnect function
    return ret;
}

// Starts thread worker
int xr25_start_worker(const XR25DiagnosticContext *context)
{
    // Sanity check
    if (context == NULL)
        return -1;

    // Start thread
    return pthread_create((pthread_t *)(&(context->diagnostic_thread)), NULL, xr25_thread_worker, (void *)(context));
}

// Dummy send request - first version do not support sending commands :(
int xr25_send_request(const XR25DiagnosticContext *context, const char *data)
{
    if (context == NULL)
        return -1;
    return context->card->send_request(context, data);
}

// Just worker
void *xr25_thread_worker(void *data)
{
    // Variables
    XR25DiagnosticContext *context;
    int ret;
    long current_time_msec;
    long current_time_usec;
    long time_diff;

    // Detach thread
    pthread_detach(pthread_self());

    // Get context
    context = (XR25DiagnosticContext *)data;

    // Endless loop
    while (context->is_connected == XR25ConnectionStatusConnected)
    {
        // Get current time in msec
        current_time_msec = xr25_get_timestamp_msec();
        current_time_usec = xr25_get_timestamp_usec();

        // Check if it is time to tick
        if (current_time_msec - context->last_tick > XR25_TICK_INTERVAL_MSEC)
        {
            // Tick data
            pthread_mutex_lock(context->diagnostic_mutex);
            context->last_tick = current_time_msec;
            context->last_tick_wall = xr25_get_timestamp_msec_wall();
            ret = context->card->tick_data(context);

            // Check result
            if (ret != 0)
            {
                context->is_connected = XR25ConnectionStatusConnectedExiting;
                pthread_mutex_unlock(context->diagnostic_mutex);
                context->thread_exit_value = ret;
                break; // Exit from loop
            }
            pthread_mutex_unlock(context->diagnostic_mutex);

            // Calculate usleep
            time_diff = (XR25_TICK_INTERVAL_MSEC * 1.0e3) - (xr25_get_timestamp_usec() - current_time_usec);

            // Sleep
            if (time_diff > 0)
                usleep(time_diff);
        }
        else
        {
            // Missed with calculation - wait 100us (to not use much CPU)
            usleep(100);
        }
    }

    // Loop ended! Time to exit and change status.
    context->is_connected = XR25ConnectionStatusConnectedThreadExited;
    pthread_exit(NULL);
}

unsigned long xr25_get_timestamp_msec()
{
    struct timespec spec;
    long time;
    long ms;

    clock_gettime(CLOCK_MONOTONIC, &spec);
    ms = round(spec.tv_nsec / 1.0e6); // convert nsec to msec
    time = spec.tv_sec * 1.0e3;        // convert sec to msec
    time += ms;                       // add

    return time;
}

unsigned long xr25_get_timestamp_usec()
{
    struct timespec spec;
    long time;
    long us;

    clock_gettime(CLOCK_MONOTONIC, &spec);
    us = round(spec.tv_nsec / 1.0e3); // convert nsec to usec
    time = spec.tv_sec * 1.0e6;        // convert sec to usec
    time += us;                       // add

    return time;
}

unsigned long xr25_get_timestamp_msec_wall()
{
    struct timespec spec;
    long time;
    long ms;

    clock_gettime(CLOCK_REALTIME, &spec);
    ms = round(spec.tv_nsec / 1.0e6); // convert nsec to msec
    time = spec.tv_sec * 1.0e3;        // convert sec to msec
    time += ms;                       // add

    return time;
}

unsigned long xr25_get_timestamp_usec_wall()
{
    struct timespec spec;
    long time;
    long us;

    clock_gettime(CLOCK_REALTIME, &spec);
    us = round(spec.tv_nsec / 1.0e3); // convert nsec to usec
    time = spec.tv_sec * 1.0e6;        // convert sec to usec
    time += us;                       // add

    return time;
}