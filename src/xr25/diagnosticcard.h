#ifndef __XR25DIAGNOSTICCARD__
#define __XR25DIAGNOSTICCARD__

#include "config.h"
#include "diagnosticcontext.h"

// Circular struct dependency
typedef struct XR25DiagnosticContext XR25DiagnosticContext;

// Computer type definition
typedef enum XR25ComputerType
{
    XR25ComputerTypeDashboard, // In Renault Safrane dashboard do not have KLine diagnostics it is used for voice synthesizer!!
    XR25ComputerTypeCruiseControl,
    XR25ComputerTypePetrolInjection,
    XR25ComputerTypeDiselInjection
    // Add more
} XR25ComputerType;

// Cable type definition
typedef enum XR25CableType
{
    XR25CableTypeCableS1ODB1,
    XR25CableTypeCableS2ODB1,
    XR25CableTypeCableS3ODB1,
    XR25CableTypeCableS4ODB1,
    XR25CableTypeCableS5ODB1,
    XR25CableTypeCableS6ODB1,
    XR25CableTypeCableS7ODB1,
    XR25CableTypeCableS67ODB1,
    XR25CableTypeCableS8ODB1,
    XR25CableTypeCableS8ODB2,
    XR25CableTypeCableS8ODB12

} XR25CableType;

// Car manufactrer definition
typedef enum XR25CarManufacturer
{
    XR25CarManufacturerRenault
} XR25CarManufacturer;

// Car model definitions
typedef enum XR25CarModel
{
    XR25CarModelRenault1X54SafranePh1,
    XR25CarModelRenault1X54SafranePh2,
    XR25CarModelRenault1X56Laguna1Ph1,
    XR25CarModelRenault1X56Laguna1Ph2
} XR25CarModel;

// Parameter for diagnostic card definition
typedef struct XR25DiagnosticCardParameter
{
    const char name[XR25_DIAGNOSTIC_CARD_PARAMETER_NAME_LENGTH];
    const char label[XR25_DIAGNOSTIC_CARD_PARAMETER_NAME_LENGTH];
    const char unit[XR25_DIAGNOSTIC_CARD_PARAMETER_UNIT_LENGTH];
    int (*get_value)(XR25DiagnosticContext *context, char *output_string);
} XR25DiagnosticCardParameter;

// Diagnostic card definition
typedef struct XR25DiagnosticCard
{
    const char code[XR25_DIAGNOSTIC_CARD_CODE_LENGTH]; // Use _ as space, only 5 chars + NULL
    const char unique_code[XR25_DIAGNOATIC_CARD_UNIQUE_CODE_LENGTH];
    const XR25ComputerType type;
    const XR25CarManufacturer car_manufacturer;
    const XR25CarModel car_model;
    const XR25CableType cable_type;
    const int baud_rate;
    const int address;
    const int calculator_id;
    const int priv_data_size;
    const XR25DiagnosticCardParameter *const parameter_list[XR25_DIAGNOSTIC_CARD_PARAMETER_COUNT];
    void (*initialize)(const XR25DiagnosticContext *context);
    int (*connect)(const XR25DiagnosticContext *context);
    int (*disconnect)(const XR25DiagnosticContext *context);
    int (*tick_data)(const XR25DiagnosticContext *context);
    int (*send_request)(const XR25DiagnosticContext *context, const char *data);
    void (*deinitialize)(const XR25DiagnosticContext *context);
} XR25DiagnosticCard;

// Functions related to diagnostic cards
const char *xr25_get_computer_type_string(XR25ComputerType computer);
const char *xr25_get_cable_type_string(XR25CableType cable);
const char *xr25_get_car_manufacturer_string(XR25CarManufacturer manufacturer);
const char *xr25_get_car_model_string(XR25CarManufacturer manufacturer, XR25CarModel model);

// Functions related to diagnostic card list
int xr25_get_diagnotic_card_count();
const XR25DiagnosticCard **xr25_get_diagnostic_card_list();
const XR25DiagnosticCard *xr25_get_diagnostic_card_by_code(const char *code);
const XR25DiagnosticCard *xr25_get_diagnostic_card_by_unique_code(const char *unique_code);
const XR25DiagnosticCard *xr25_get_diagnostic_card_by_index(int index);

#endif //__XR25DIAGNOSTICCARD__