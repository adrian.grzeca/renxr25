#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <stdlib.h>
#include <locale.h>

#include "application.h"

int main(int argc, char **argv)
{
    // Variables
    ApplicationContext *application_context;
    GtkApplication *application;
    int status;

    // Setting up locale
    setlocale(LC_ALL, "");
    bindtextdomain(GETTEXT_DOMAIN, GETTEXT_OUTPUT_DIR);
    bind_textdomain_codeset(GETTEXT_DOMAIN, "UTF-8");
    textdomain(GETTEXT_DOMAIN);

    // Creating GTK application
    application_context = malloc(sizeof(ApplicationContext));
    application_init(application_context);
    application = gtk_application_new("me.grzeca.renxr25", G_APPLICATION_FLAGS_NONE);
    if (application == NULL || application_context == NULL)
    {
        g_error("Early initialize error.. Exiting");
        return 1;
    }
    g_signal_connect(application, "activate", G_CALLBACK(application_activate), application_context);
    status = g_application_run(G_APPLICATION(application), argc, argv);

    // Free-up objects
    g_object_unref(application);
    application_deinit(application_context);
    free(application_context);

    return status;
}