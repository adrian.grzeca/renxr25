#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <ftdi.h>
#include <libusb.h>

#include "interfaceselectwindow.h"
#include "application.h"
#include "config.h"

static const char invalid_manufacturer[] = "??";
static const char invalid_description[] = "??";
static const char invalid_serial[] = "??";

// Declare stupid interface GObject class
struct _RX25Interface
{
    GObject parent;
    char name[STRING_BUFFER_SIZE];
    int bus;
    int port;
};

G_DEFINE_TYPE(RX25Interface, rx25_interface, G_TYPE_OBJECT)

static void
rx25_interface_class_init(RX25InterfaceClass *class)
{
}

static void
rx25_interface_init(RX25Interface *self)
{
}

int rx25_interface_get_bus(RX25Interface *self)
{
    g_return_val_if_fail(RX25_IS_INTERFACE(self), 0);
    return self->bus;
}

int rx25_interface_get_port(RX25Interface *self)
{
    g_return_val_if_fail(RX25_IS_INTERFACE(self), 0);
    return self->port;
}

const char *rx25_interface_get_name(RX25Interface *self)
{
    g_return_val_if_fail(RX25_IS_INTERFACE(self), NULL);
    return self->name;
}

RX25Interface *rx25_interface_new(const char *name, int bus, int port)
{
    RX25Interface *self;

    self = g_object_new(RX25_TYPE_INTERFACE, NULL);
    strncpy(self->name, name, STRING_BUFFER_SIZE);
    self->bus = bus;
    self->port = port;

    return self;
}

void interfaceselectwindow_show(ApplicationContext *context)
{
    // Variables
    char buffer[STRING_BUFFER_SIZE], manufacturer[STRING_BUFFER_SIZE], description[STRING_BUFFER_SIZE], serial[STRING_BUFFER_SIZE];
    int ret;
    uint8_t bus, port;
    GtkWindow *window_interface_select;
    GtkBox *box_main, *box_button;
    GtkLabel *label_information1, *label_information2;
    GtkScrolledWindow *scrolledwindow;
    GtkListView *listview;
    GtkListItemFactory *listitemfactory;
    GtkSingleSelection *selectionmodel;
    GListStore *liststore_device_list;
    GtkButton *button_ok, *button_cancel;
    GtkMessageDialog *dialog_ftdi_error;
    struct ftdi_device_list *ftdi_dev_list, *curdev;
    InterfaceSelectionContext *selection_context;

    // Create device list
    ret = ftdi_usb_find_all(context->ftdi, &ftdi_dev_list, 0, 0);
    if (ret < 0)
    {
        switch (ret)
        {
        case -3:
            g_critical("FTDI USB find all fail. Out of memory.");
            break;
        case -5:
            g_critical("FTDI USB find all fail. Get device list failed.");
            break;
        case -6:
            g_critical("FTDI USB find all fail. Get device descriptor failed.");
            break;
        default:
            g_critical("FTDI USB find all fail. Without reason.");
            break;
        }
        dialog_ftdi_error = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(context->window_main,
                                                                      GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                                      GTK_MESSAGE_ERROR,
                                                                      GTK_BUTTONS_OK,
                                                                      _("fatal_error")));
        gtk_message_dialog_format_secondary_text(dialog_ftdi_error, _("ftdi_error_find_usb"));
        g_signal_connect(dialog_ftdi_error, "response", G_CALLBACK(application_exit_dialog_signal), context);
        gtk_window_present(GTK_WINDOW(dialog_ftdi_error));
        return;
    }

    // Populate list
    liststore_device_list = g_list_store_new(RX25_TYPE_INTERFACE);
    for (curdev = ftdi_dev_list; curdev != NULL; curdev = curdev->next)
    {
        // Get device physical address
        bus = libusb_get_bus_number(curdev->dev);
        port = libusb_get_device_address(curdev->dev);

        // Get manufacturer
        ret = ftdi_usb_get_strings(context->ftdi, curdev->dev, manufacturer, STRING_BUFFER_SIZE, NULL, 0, NULL, 0);
        if (ret != 0 && ret != -7)
        {
            // Skip device
            g_warning("Skiping interface 0x%02x / 0x%02x.", bus, port);
            continue;
        }
        if (ret == -7)
        {
            strcpy(manufacturer, invalid_manufacturer);
        }

        // Get description
        ret = ftdi_usb_get_strings(context->ftdi, curdev->dev, NULL, 0, description, STRING_BUFFER_SIZE, NULL, 0);
        if (ret != 0 && ret != -8)
        {
            // Skip device
            g_warning("Skiping interface 0x%02x / 0x%02x.", bus, port);
            continue;
        }
        if (ret == -8)
        {
            strcpy(description, invalid_description);
        }

        // Get serial
        ret = ftdi_usb_get_strings(context->ftdi, curdev->dev, NULL, 0, NULL, 0, serial, STRING_BUFFER_SIZE);
        if (ret != 0 && ret != -9)
        {
            // Skip device
            g_warning("Skiping interface 0x%02x / 0x%02x.", bus, port);
            continue;
        }
        if (ret == -9)
        {
            strcpy(serial, invalid_serial);
        }

        // Append it to list
        g_snprintf(buffer, STRING_BUFFER_SIZE, "%s %s (0x%02x:0x%02x)", manufacturer, description, bus, port);
        g_list_store_append(liststore_device_list, rx25_interface_new(buffer, bus, port));
    }
    ftdi_list_free(&ftdi_dev_list);

    // Create window
    window_interface_select = GTK_WINDOW(gtk_window_new());
    gtk_window_set_transient_for(window_interface_select, GTK_WINDOW(context->window_main));
    gtk_window_set_modal(window_interface_select, TRUE);
    gtk_window_set_resizable(window_interface_select, FALSE);
    gtk_window_set_default_size(window_interface_select, 400, 500);
    gtk_window_set_title(window_interface_select, _("title_interface_select"));

    // Create box
    box_main = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_main), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_main), 5);
    gtk_window_set_child(window_interface_select, GTK_WIDGET(box_main));

    // Create label information #1
    label_information1 = GTK_LABEL(gtk_label_new(NULL));
    g_snprintf(buffer, STRING_BUFFER_SIZE, "<span size=\"x-large\"><b>%s</b></span>", _("label_please_select_interface"));
    gtk_label_set_markup(label_information1, buffer);
    gtk_box_append(box_main, GTK_WIDGET(label_information1));

    // Create label information #2
    label_information2 = GTK_LABEL(gtk_label_new(NULL));
    gtk_label_set_markup(label_information2, _("label_please_select_interface_note"));
    gtk_box_append(box_main, GTK_WIDGET(label_information2));

    // Create scrolled window
    scrolledwindow = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new());
    gtk_widget_set_hexpand(GTK_WIDGET(scrolledwindow), TRUE);
    gtk_widget_set_vexpand(GTK_WIDGET(scrolledwindow), TRUE);
    gtk_widget_set_size_request(GTK_WIDGET(scrolledwindow), -1, 300);
    gtk_box_append(box_main, GTK_WIDGET(scrolledwindow));

    // Create list view
    selectionmodel = gtk_single_selection_new(G_LIST_MODEL(liststore_device_list));
    listitemfactory = gtk_signal_list_item_factory_new();
    g_signal_connect(listitemfactory, "setup", G_CALLBACK(interfaceselectwindow_factory_setup_signal), NULL);
    g_signal_connect(listitemfactory, "bind", G_CALLBACK(interfaceselectwindow_factory_bind_signal), NULL);
    listview = GTK_LIST_VIEW(gtk_list_view_new(GTK_SELECTION_MODEL(selectionmodel), listitemfactory));
    gtk_scrolled_window_set_child(scrolledwindow, GTK_WIDGET(listview));

    // Create box for buttons
    box_button = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5));
    gtk_widget_set_margin_top(GTK_WIDGET(box_button), 5);
    gtk_widget_set_margin_bottom(GTK_WIDGET(box_button), 5);
    gtk_widget_set_margin_start(GTK_WIDGET(box_button), 5);
    gtk_widget_set_margin_end(GTK_WIDGET(box_button), 5);
    gtk_box_append(box_main, GTK_WIDGET(box_button));

    // Create selection context
    selection_context = malloc(sizeof(InterfaceSelectionContext));
    selection_context->application_context = context;
    selection_context->window_interface_select = window_interface_select;
    selection_context->selectionmodel = selectionmodel;

    // Create button ok
    button_ok = GTK_BUTTON(gtk_button_new());
    gtk_button_set_label(button_ok, _("button_ok"));
    gtk_widget_set_hexpand(GTK_WIDGET(button_ok), TRUE);
    gtk_box_append(box_button, GTK_WIDGET(button_ok));
    g_signal_connect(button_ok, "clicked", G_CALLBACK(interfaceselectwindow_ok_signal), selection_context);

    // Create button cancel
    button_cancel = GTK_BUTTON(gtk_button_new());
    gtk_button_set_label(button_cancel, _("button_cancel"));
    gtk_widget_set_hexpand(GTK_WIDGET(button_cancel), TRUE);
    gtk_box_append(box_button, GTK_WIDGET(button_cancel));
    g_signal_connect(button_cancel, "clicked", G_CALLBACK(interfaceselectwindow_cancel_signal), window_interface_select);

    // Show window
    g_signal_connect(button_cancel, "destroy", G_CALLBACK(interfaceselectwindow_destroy_signal), selection_context);
    gtk_widget_show(GTK_WIDGET(window_interface_select));
}

void interfaceselectwindow_destroy_signal(GtkWidget *self, gpointer user_data)
{
    // Destroys selection context
    free(user_data);
}

void interfaceselectwindow_cancel_signal(GtkButton *button, gpointer user_data)
{
    GtkWindow *window_interface_select;
    window_interface_select = GTK_WINDOW(user_data);
    gtk_window_destroy(window_interface_select);
}

void interfaceselectwindow_ok_signal(GtkButton *button, gpointer user_data)
{
    // Variables
    InterfaceSelectionContext *selection_context;
    ApplicationContext *application_context;
    GtkWindow *window_interface_select;
    GtkSingleSelection *selection_model;
    RX25Interface *object;
    GtkMessageDialog *dialog_box;
    const char *string;

    // Copy pointers from temporary context
    selection_context = (InterfaceSelectionContext *)user_data;
    application_context = selection_context->application_context;
    window_interface_select = selection_context->window_interface_select;
    selection_model = selection_context->selectionmodel;

    // Get selected interface
    object = RX25_INTERFACE(gtk_single_selection_get_selected_item(selection_model));
    string = rx25_interface_get_name(object);

    // Set interface and close window
    application_context->interface_bus = rx25_interface_get_bus(object);
    application_context->interface_port = rx25_interface_get_port(object);
    gtk_label_set_label(application_context->label_interface_name, string);
    gtk_window_destroy(window_interface_select);

    // Log to console
    g_critical("Selected interface BUS=0x%02x PORT=0x%02x NAME=%s",
               application_context->interface_bus,
               application_context->interface_port,
               string);

    // Show message box
    dialog_box = GTK_MESSAGE_DIALOG(gtk_message_dialog_new(application_context->window_main,
                                                           GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                           GTK_MESSAGE_INFO,
                                                           GTK_BUTTONS_OK,
                                                           _("success_error")));
    gtk_message_dialog_format_secondary_text(dialog_box, "%s: %s", _("selected_interface_info"), string);
    g_signal_connect(dialog_box, "response", G_CALLBACK(gtk_window_destroy), NULL);
    gtk_window_present(GTK_WINDOW(dialog_box));
}

void interfaceselectwindow_factory_setup_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
    GtkWidget *label;
    label = gtk_label_new(NULL);
    gtk_list_item_set_child(list_item, label);
}

void interfaceselectwindow_factory_bind_signal(GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
    GtkWidget *label;
    RX25Interface *object;
    label = gtk_list_item_get_child(list_item);
    object = gtk_list_item_get_item(list_item);
    gtk_label_set_text(GTK_LABEL(label), rx25_interface_get_name(object));
}