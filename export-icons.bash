#!/bin/bash

resolutions=("8" "16" "22" "24" "32" "36" "42" "44" "48" "64" "72" "96" "128" "150" "192" "256" "310" "512")

for i in ${!resolutions[*]}; do
    current_resolution=${resolutions[$i]}
    echo "Doing: $current_resolution"
    inkscape -w "$current_resolution" -h "$current_resolution" "assets/renxr25-icon.svg" -o "assets/renxr25-icon-${current_resolution}x${current_resolution}.png"
done